Support vector machines (SVM) are based in splitting the data using geometrical figures to classify it. The most simple SVM is a line on a 2D dataset. The data above the line belongs to one category and below to other.

In a $p$ dimensional space, this "line" is called **hyperplane**, and has $p-1$. A bit of reasoning is enough to understand how a $p-1$ body splits a $p$ dimensional space in two.

The hyperplane is defined as follows:

$$
\beta x_0 + \beta x_1 + \dots + \beta x_{p-1} = 0
$$

The points considered to build the hyperplane are known as **support vectors**, hyperparameters determine how many support vectors are needed.

A dataset can have different hyperplanes that split the data, it is important to choose the one that splits it better, so the model won't get confused with new data; this is, the hyperplane more separate from each observation, it is known as **maximal margin hyperplane**.

SVM are useful when the dataset is small and has a lot of features.

For more information see [Support Vector Machines Succintly](https://www.syncfusion.com/succinctly-free-ebooks/support-vector-machines-succinctly). (116 pages).

## Soft Margin SVM

Since we live in a real world, data cannot be just splitted by a hyperplane, and finding the maximal margin hyperplane, is computationally very expensive.

Soft Margin SVM allow some observations to be in the wrong side of the **margin** or even the hyperplane, the **margin** is the distance from the closest points (vectors) to the hyperplane. Only the points inside the margin are considered as *support vectors*. The size of the margin is controlled with the $C$ hyperparameter. We must optimize the hyperplane with the maximum margin and minimum errors.

**C Hyperparameter:** The smaller $C$ the wider the margin.

## SVM Kernel

Sometimes (almost always), a dataset is not separable in its dimensional space, but adding a new dimension makes it separable. A new dimension can be formed by combining any of its original dimensions using a transformation.

The **kernel** is a function that returns the dot product of two vectors in a new dimensional space, in other words, a *transformation*. There are different transformations that allow the data to be linearly separated, but not all of them are **kernels**. The support vectors and hyperplane can be obtained from the kernel. This is known as the [kernel trick](https://towardsdatascience.com/the-kernel-trick-c98cdbcaeb3f).

There are different kernels, the most common are:
- **Linear kernel**
$$
k(x, x') = x\cdot x'
$$
- **Polynomial kernel**
$$
k(x, x') = (x\cdot x' + c)^d
$$
- **Gaussian kernel (Radial Basis Function)**
$$
k(x, x') = \exp(-\gamma || x - x' ||^2)
$$

### Kernel trick

Transforming all the data is sometimes too expensive. So, the kernel trick consists in representing the data through a set of similarity comparisons between the original features, instead of actually applying the transformation. It is often represented as a matrix, where the position $[i, j]$ represents the comparison between the feature $i$ and the feature $j$.

## Multi-class Classification

### One-versus-one SVM

A SVM is created for each pair of classes, when a new observation is presented, it is compared with each SVM, the class it belongs is the class where it fell more times.

This is very expensive when having many classes.

### One-versus-all SVM

Each class is compared to the rest of the classes as a whole. This can have a lot of bias.

### DAGSVM

Directed Acyclic Graph SVM is an improvement to one-versus-one. See the one-versus-one as a complete graph, turn it into a DAG and do the same. It improves the performance.

## [Sklearn](Must%20Have%20Libraries%20(Python).md#scikit-learn) SVM

[SVC](https://scikit-learn.org/stable/modules/generated/sklearn.svm.SVC.html) for classification. Uses different kernels.
[Linear SVC](https://scikit-learn.org/stable/modules/generated/sklearn.svm.LinearSVC.html#sklearn.svm.LinearSVC) for classification, uses a faster algorithm.
[SVR](https://scikit-learn.org/stable/modules/generated/sklearn.svm.SVR.html#sklearn.svm.SVR) for regression.

```python
from sklearn.svm import SVC

model = SVC(C = 100, kernel = 'linear', random_state=123)
model.fit(x, y)
model.support_vectors_
```

The available kernels are `linear`, `rbd`, `poly`.