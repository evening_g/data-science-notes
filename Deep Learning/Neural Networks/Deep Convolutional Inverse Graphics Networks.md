---
aliases:
  - Deep Convolutional Autoencoder
---
These are similar to [[Variational Autoencoders]] but use a [[Convolutional Neural Networks|CNN]] as encoder and [[Deconvolutional Neural Networks|DNN]] as decoder. Usually are trained via [[Backpropagation]].

These networks are mainly used **generating graphics** and are very good at creating data that they've never been trained with. Can also be used to **edit graphics**.

![[dcign.png]]

[Deep Convolutional Inverse Graphics Networks](https://arxiv.org/pdf/1503.03167v4.pdf)
