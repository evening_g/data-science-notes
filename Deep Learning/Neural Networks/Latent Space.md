This is a lower-dimensional representation of the data. 

Latent space can be obtained through an [[Autoencoders|encoder net]]. It is necessary that the data has dependencies across dimensions, else, each dimension will be independent and won't be possible to be reduced.
