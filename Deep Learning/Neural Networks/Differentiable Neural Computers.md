This is a variation of [[Neural Turing Machines]] with more memory. It replaces the CPU in the [von Neumann architecture](https://en.wikipedia.org/wiki/Von_Neumann_architecture) with a [[Recurrent Neural Networks|RNN]].

![[dnc.png]]

[raves, Alex, et al. “Hybrid computing using a neural network with dynamic external memory.” Nature 538 (2016)](https://www.nature.com/articles/nature20101)