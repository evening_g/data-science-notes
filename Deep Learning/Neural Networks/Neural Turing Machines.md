This is an abstraction of [[Long Short-Term Memory Networks]] that includes a memory cell and can behave as a Turing Machine.

![[ntm.png]]

[Graves, Alex, Greg Wayne, and Ivo Danihelka. “Neural turing machines.” arXiv preprint arXiv:1410.5401 (2014)](Graves, Alex, Greg Wayne, and Ivo Danihelka. “Neural turing machines.” arXiv preprint arXiv:1410.5401 (2014).)
