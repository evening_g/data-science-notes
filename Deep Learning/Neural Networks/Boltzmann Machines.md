These are similar to [[Red Hopfield Networks]] but some neurons are hidden neurons and others are input/output neurons. It uses [[Markov Chains]] to determine the weights of the neurons. These are stochastic networks. They have a "global temperature" which controls the sum of the activation of all the neurons. The network converges with the adequate temperature.

![[bm.png]]