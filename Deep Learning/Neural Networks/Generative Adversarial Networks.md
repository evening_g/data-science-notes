These are two "adversary" neural networks, usually a combination of [[Multilayer Perceptron|FFNN]] and a [[Convolutional Neural Networks|CNN]]. One has the task to generate content and the other to judge it. When training, the generator network improves at generating content and the judge network generates at judging. They're hard to train as they may never converge.

![[gan.png]]

[Generative Adversarial Nets](https://arxiv.org/pdf/1503.03167v4.pdf)
