---
aliases:
  - Self-organizing map
---
These networks are used to **classify data** [[Unsupervised Learning|unsupervised]]. 

![[kn.png]]

[Kohonen, Teuvo. “Self-organized formation of topologically correct feature maps.” Biological cybernetics 43.1 (1982)](http://cioslab.vcu.edu/alg/Visualize/kohonen-82.pdf)
