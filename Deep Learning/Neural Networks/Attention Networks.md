This includes the [[Transformer Architecture]]. They use an "attention" mechanism to counteract the information deterioration. It stores previous states of the networks and changes focus between states.

![[an.png]]

[Jaderberg, Max, et al. “Spatial Transformer Networks.” In Advances in neural information processing systems (2015): 2017-2025.  
[Artículo original PDF](https://arxiv.org/pdf/1506.02025.pdf)](https://arxiv.org/pdf/1506.02025.pdf)