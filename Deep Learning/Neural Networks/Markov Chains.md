This are very different from other neural networks. They don't have memory. They work on the basis, from this point, what is the node to go to the neighbors nodes?. Sometimes they're not completely connected, in this case they can be trained by [[Contrastive Divergence]].

![[mc.png]]

[Markov Chain Paper](https://www.americanscientist.org/sites/americanscientist.org/files/201321152149545-2013-03Hayes.pdf)