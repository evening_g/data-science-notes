This is a variation of [[Recurrent Neural Networks]] but their neurons are also connected to the "future". It can be used to fill a gap in an image for example.

[Bidirectional Recurrent Neural Networks Paper](https://maxwell.ict.griffith.edu.au/spl/publications/papers/ieeesp97_schuster.pdf).

