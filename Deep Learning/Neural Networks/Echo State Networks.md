This is another recurrent network. During the training, the connections of the neurons are changed.

![[esn.png]]

[Jaeger, Herbert, and Harald Haas. “Harnessing nonlinearity: Predicting chaotic systems and saving energy in wireless communication.” science 304.5667 (2004)](https://www.sciencedirect.com/science/article/abs/pii/S0925231206000385?via%3Dihub)
