This is a variation of [[Long Short-Term Memory Networks]]. They have a **update** and a **reset** gate. The **update** gate determines how much information take from the previous layer and how much from the previous state.

They are faster than LSTM but they are less *expressive*.

![[gru.png]]
[Original Paper](https://arxiv.org/pdf/1412.3555v1.pdf).
