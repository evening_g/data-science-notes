---
tags:
  - "#supervised_learning"
  - "#classification"
  - "#regression"
  - "#machine_learning"
---
This is a [Supervised learning](Supervised%20learning.md) model for both classification and regression.

An ANN is a collection of artificial neurons, which "model" a biological brain. Each connection transmits a signal to other neuron, then it is processed and retransmitted. The signal is a real number. The output of each neuron is computed by some non-linear function of the sum of its inputs. The neurons have **weights**.

Each neuron can solve a linearly separable problem. In a whole, it is demonstrated that $n$ number of neurons can solve any problem.

Neurons are arranged into layers. The first neurons are the input neurons, and the last ones are the output. The in between neurons are called **hidden layers.**

A **deep neural network** is an ANN that has at least 2 hidden layers.

Neural networks cannot be reversed, which means, whilst a network could identify an image as a number cat, there's no way for it to draw the image of a cat based on the information in its neurons.

The *hello world* for neural networks is digit recognition. In the MNIST database you can find a large dataset of labeled images to train your first model. Just for the fact, the highest precision achieved with a NN here is 99.79%.

Neural networks are no-parametric, which means data doesn't have to follow the [Normal Distribution](Normal%20Distribution.md), but it must be [scaled](Scalling%20the%20data.md).

For more information see:
- [Artificial Neural Networks on Wikipedia](https://en.wikipedia.org/wiki/Artificial_neural_network).
- [Neural Network and Deep Learning Book by Michael Nielsen](http://neuralnetworksanddeeplearning.com/)
- [3Blue1Brown series on Deep Learning](https://youtu.be/aircAruvnKk?si=8i7i_9uBuV02ZUR6)
## Types of Neural Networks

- [Multilayer Perceptron](Multilayer%20Perceptron.md) (Feed Forward).
- [[Adaline]] (single layer NN)
- [[Radial Basis Function Neural Network]].
- [[Recurrent Neural Networks]] (for autocomplete).
- [[Long Short-Term Memory Networks]] (for speech recognition).
- [[Gated Recurrent Units]] (fast LSTMN).
- [[Bidirectional Recurrent Neural Networks]] (filling gaps).
- [[Autoencoders]] (automatic categorization).
- [[Variational Autoencoders]] ("supervised" automatic categorization).
- [[Denoising Autoencoders]] (noise-proof autoencoder).
- [[Sparce Autoencoder]] (for extracting characteristics).
- [[Markov Chains]] (TODO for what?)
- [[Red Hopfield Networks]] (for memory association)
- [[Boltzmann Machines]] (memory association)
- [[Restricted Boltzmann Machines]] (TODO for what?)
- [[Deep Belief Networks]] (TODO for what?)
- [Convolutional Neural Network](Convolutional%20Neural%20Networks.md) (for image recognition).
- [[Deconvolutional Neural Networks]] (for image generation).
- [[Deep Convolutional Inverse Graphics Networks]] (for image generation).
- [[Generative Adversarial Networks]] (for data generation).
- [[Liquid State Machines]] (TODO for what?)
- [[Extreme Learning Machines]] (TODO for what?)
- [[Echo State Networks]] (TODO for what?)
- [[Deep Residual Networks]] (TODO for what?)
- [[Neural Turing Machines]] ("intelligent" Turing Machine)
- [[Differentiable Neural Computers]] (TODO for what?)
- [[Capsule Networks]] (TODO for what?)
- [[Kohonen Networks]] (for unsupervised classification)
- [[Attention Networks]] (TODO for what?)

## Types of Neural Networks (Images)

![[networkZooPoster.png]]