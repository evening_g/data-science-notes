Red Hopfield Networks work as everything, input, output or hidden layers. It is trained establishing the values of the neurons in a given pattern, then weights are calculated. After trained for one or more patterns the network will hopefully converge in one of the memorized patterns.

Neurons are usually updated one by one in a random sequence. When the neurons are not updated, it is that it has converged.

It is also called **associative memory network**.

![[hn.png]]
