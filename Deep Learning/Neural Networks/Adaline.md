**Ada**ptative **Li**near **Ne**uron is an early single layer [[Artificial Neural Networks|ANN]]. It is based on the [[Multilayer Perceptron]], the main difference is the way it learns.

The output of a neuron is defined as:

$$
y = \sum^{n}_{j=1} x_j w_j + \theta
$$
Where:
$x$ is the input vector.
$w$ is the weight vector.
$n$ is the number of input.
$\theta$ is a constant
$y$ is the output.

To learn in uses a *teacher* function which generates an error using [[Least Mean Squares]]. It is defined as following:
$$
e = (d - \sum xw)
$$
This error is used to update the weights in the neuron layer using a similar method to [[Backpropagation]].