These are a variation of deep [[Multilayer Perceptron|FFNN]] with additional connections that share information to subsequent layers. These networks are efficient in learning patterns. Have a similar behavior than [[Deep Residual Networks]] and [[Long Short-Term Memory Networks]].

![[drn.png]]

[He, Kaiming, et al. “Deep residual learning for image recognition.” arXiv preprint arXiv:1512.03385 (2015)](https://arxiv.org/pdf/1512.03385v1.pdf)

