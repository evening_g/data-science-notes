Are similar to [[Echo State Networks]]. They have [[Spiking Neurons]]. A [[Activation Functions#Threshold function|Threshold function]] is used and each neuron has also cumulative memory, only when the value in the neuron reaches the threshold, it shares its value to the neighbor neurons. It's similar to the process of filling an ice cube tray.

![[lsm.png]]

[Real-Time Computing Without Stable States: A new Framework for Neural COmputation Based on Perturbations](https://web.archive.org/web/20120222154641/http://ramsesii.upf.es/seminar/Maass_et_al_2002.pdf).
