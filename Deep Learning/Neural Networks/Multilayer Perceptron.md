---
aliases:
  - Feedforward Neural Network (FNN)
---
This is also known as **Feedforward Neural Network (FNN)**. It was invented in the 80's.

This is one of the two major types of [ANN](Artificial%20Neural%20Networks.md), the flow of the information between layers is uni-directional. This in contrast of [Recurrent Neural Networks](Recurrent%20Neural%20Networks.md).

The process of propagating the data from the input to the output is known as the **feedforward step**.

FNN are trained using [Backpropagation](Backpropagation.md).

## Usage

The perceptron is available in [scikit-learn](Must%20Have%20Libraries%20(Python).md#scikit-learn).

```python
from sklearn.neural_network import MLPClassifier
from sklearn.neural_network import MLPRegressor

# replace for classifier
model = MLPRegressor(
     hidden_layer_sizes = (5, 19, 5),
     solver = "lbfgs",
     max_iter = 1000,
     random_state = 123,
     activation='relu' #tanh logistic
)

model.fit(x_train, y_train)

model.intercepts_ # i-th element is the bias vector to layer i+1
model.coefs_ #i-th element is the weight matrix of layer i
```

https://scikit-learn.org/stable/modules/generated/sklearn.neural_network.MLPClassifier.html
https://scikit-learn.org/stable/modules/generated/sklearn.neural_network.MLPRegressor.html

A better version is available in [Keras](Must%20Have%20Libraries%20(Python).md#Keras).

```python
from keras.models import Sequential
from keras.layers import Dense
from keras.utils. import set_random_seed

# the perceptron is called `Sequential`
model = Sequential()
set_random_seed(123)
model.add(Dense(70, activation='tanh')) # add a layer with 70 neurons and tanh activation
model.add(Dense(1, activation='relu')) # add a layer with 1 neuron and ReLU activation

model.compile(loss='mean_squared _error', # loss function to use
              opmimizer='adam' # optimizer function to use https://keras.io/api/optimizers/
              metrics=['binary_accuracy']
             )

model.fit(x_train, y_train, epochs=N) # number of epochs (iterations)
scores = model.evaluate(x_test, y_test)

# get the weights and biases of the neurons in the 0-th hidden layer
weights, biases = model.layers[0].get_weights()
```

This can be used as binary. Use 0.5 as the threshold.


### Net visualization

The package [VizNet](Must%20Have%20Libraries%20(Python).md#VizNet) provides some functions for it.

```python
from viznet import connecta2a, node_sequence, NodeBrush, EdgeBrush, DynamicShow

def draw_feed_forward(ax, num_node_list):
    """
    draws a feed forward neural network

    num_node_list (list<int>): number of nodes in each layer
    """

    num_hidden_layer = len(num_node_list) - 2
    kind_list = ['nn.input'] + ['nn.hidden'] * num_hidden_layer + ['nn.output']
    radius_list = [0.3] + [0.1] * num_hidden_layer + [0.3]
    y_list = 10.5 * np.arange(len(num_node_list))

    seq_list = list()
    for n, kind, radius, y in zip(num_node_list, kind_list, radius_list, y_list):
        b = NodeBrush(kind, ax)
        seq_list.append(node_sequence(b, n, center=(0, y)))

    eb = EdgeBrush('-->', ax)
    for st, et in zip(seq_list[:-1], seq_list[1:]):
        connecta2a(st, et, eb)

with DynamicShow((WIDTH, HEIGHT), "file") as ds:
    draw_feed_forward(ds.ax, num_node_list=[])
```
### Hyperparameters adjustment

- **Number of layers:** Affects directly the training time and performance of the model.
- **Learning rate:** How big are the changes in the neuron parameters. A big learning rate can result in the parameters jumping from one side to another without reaching a good configuration; a small learning rate can result in the model taking a long time to reach a good configuration.
- **Optimization algorithm:** While [Stochastic Gradient Boosting](Stochastic%20Gradient%20Boosting.md) is the most popular, it can get stuck, so [L-BFGS](https://en.wikipedia.org/wiki/Limited-memory_BFGS) is recommended for small datasets, and [Adam](https://optimization.cbe.cornell.edu/index.php?title=Adam) or [RMSProp](https://optimization.cbe.cornell.edu/index.php?title=RMSProp) for large datasets.
- **Regularization:** This methods are used to reduce overfitting. You can use Weight decay or Dropout.
## What are neurons

In FNN, a neuron holds a real number between 0 and 1. This number is called **activation state.** Activation in one layer determines the activation of the next layer. In the output layer, the response is the neuron with the highest activation number.

Each neuron, also has a **weight**, this weight determines how important is that neuron, and the training process involves adjusting the weights of all the neurons in the network. The weight is a real number, usually between 0 and 1.

Neurons also have a **bias**, which helps us to better adjust the importance of the neuron. It basically says how big the activation of a neuron must be, before being actually active.

The activation of a neuron is usually represented as:

$$
a^{(1)}_0 = \sigma(w_{0,0}a_0^{(0)} + w_{0,1}a_1^{(0)} + \dots + w_{0,n}a_n^{(0)} + b_0)
$$
Where:
$a^{(i)}_j$ is the activation state of the neuron $j$ in the layer $i$.
$w$ the weight of the neuron in layer $i$, position $j$.
$b$ the bias.
$\sigma$ is the sigmoid function, but other [activation functions](Activation%20Functions.md) might be used.

This can also be represented in a matrix.

$$
\sigma\left(

\begin{bmatrix}
w_{0,0} & w_{0,1} & \dots & w_{0,n}
\\
w_{1,0} & w_{1,1} & \dots & w_{1,n}
\\
\vdots & \vdots & \ddots & \vdots
\\
w_{n,0} & w_{n,1} & \dots & w_{n,n}
\end{bmatrix}

\begin{bmatrix}
a_0^{(0)}\\
a_1^{(0)}\\
\vdots\\
a_n^{(0)}\\
\end{bmatrix}

+

\begin{bmatrix}
b_0\\
b_1\\
\vdots\\
b_n
\end{bmatrix}

\right)
$$
And therefore.
$$
a^{(1)} = \sigma(Wa^{(0)} + b)
$$
## Training the model (Theory)

The process of making the network *learn* is adjusting all this parameters so they produce a valid output.

The **cost of the function** is the sum of square of the differences between the output vector and the expected output vector (MSE). After finding the average of all this sums, we find the **total cost of the network.** Other [Evaluation Metrics](Evaluation%20Metrics.md) can be used, for example, MAE would result in a better performance over outliers. The calculus in this document are made using the MSE.

In general, training the model consists in finding the parameters that produce the minimum cost of the function. In a single parameter, this can be seen as finding the lowest point of a function, which can be easily done with the derivative. But neural networks use to have thousands of parameters, thus we need the [Gradient](Gradient.md), by gradually taking small steps in the invert direction of the gradient, we are approaching to a minimum point, this is called [Gradient descend](Gradient.md#Gradient%20descend).

Numerically finding the lowest point of a function can be done [numerically](Solving%20Non%20Linear%20Equations.md), since what we are expecting to find is $f'(x) = 0$, where $f'(x-\Delta x) < 0$ and $f'(x + \Delta x) > 0$. As numerically solving an equation doesn't guarantee finding all the solutions, and the solution obtained depends largely on the random start point; this doesn't guarantee that this is actually the *lowest* point of the function, but it is usually accurate enough.

### Backpropagation

The algorithm to find the best parameters of every neuron is called backpropagation.

After finding the average difference between the output vector and the expected output vector from all the tests, we can find how we must change the values of the previous neurons. The difference tells us the direction in with we must make the change, and the magnitude of it. The values that we can change are the bias, the weight and the activation state of the previous neuron (see neuron mathematical representation above).

This average difference is actually an approximation of the inverse gradient. Here a more mathematical point of view of it:

$$
\nabla C(w_1, b_1, w_2, b_2, \dots w_m, b_m) = 
\begin{bmatrix}
\frac{1}{m}\sum c_0
\\
\frac{1}{m}\sum c_1
\\
\vdots
\\
\frac{1}{m}\sum c_n
\end{bmatrix}
$$
Where $m$ is the number of test cases and $n$ the number of output neurons and

$$
C_i = (a_i^{(L)} - y_i)^2
$$
Where $L$ is the number of layers and $y$ is the expected output and

$$
a^{(L)} = \sigma(w^{(L)}a^{L-1} + b^{(L)}) = \sigma(z^{(L)})
$$

Since the datasets can be quite large, finding this gradient descent step is too expensive, so the data is divided into *mini-batches* producing a [Stochastic gradient descent](https://towardsdatascience.com/batch-mini-batch-stochastic-gradient-descent-7a62ecba642a) which is an approximation of the actual gradient descent step.

The activation state of an output neuron is defined as a weighted sum of all the neurons it depends on.

**The math behind**

To correctly adjust the weights and bias of the neurons, we must understand how much $C$ changes respect $w$, and respect $b$.

In other words, we're looking for the derivatives.

$$
\frac{\delta C_0}{\delta w^{(L)}}
=
\frac{\delta z^{(L)}}{\delta w^{(L)}}\frac{\delta a^{(L)}}{\delta z^{(L)}} \frac{\delta C_0}{\delta a^{(L)}}
$$
and
$$
\frac{\delta C_0}{\delta b^{(L)}}
$$

Then
$$
\frac{\delta C_0}{\delta a^{(L)}}
=
2(a^{(L)} - y)
$$
$$
\frac{\delta a^{(L)}}{\delta z^{(L)}}
=
\sigma'(z^{(L)})
$$
$$
\frac{\delta z^{(L)}}{\delta w^{(L)}} = a^{(L-1)}
$$
Which comes from:
$$
C_0 = (a^{(L)}-y)^2
$$
$$
z^{(L)} = w^{(L)}a^{(L-1)}
$$
$$
a^{(L)} = \sigma(z^{(L)})
$$
Completing the chain rule:
$$
\frac{\delta C_0}{\delta w^{(L)}} = 2a^{(L-1)}\sigma'(z^{(L)})(a^{(L)}-y)
$$
This is only for one training example, so averaging:
$$
\frac{\delta C_0}{\delta w^{(L)}}
=
\frac{1}{n}\sum_{k=0}^{n-1}\frac{\delta C_k}{\delta w^{(L)}}
$$
For the derivatives respect $b$

$$
\frac{\delta C_0}{\delta b^{(L)}} = 2\sigma'(z^{(L)})(a^{(L-1)}-y)
$$
And respect $a^{(L-1)}$:

$$
\frac{\delta C_0}{\delta a^{(L-1)}} = 2w^{(L)}\sigma'(z^{(L)})(a^{(L-1)}-y)
$$

Then we have the gradient vector.
$$
\nabla C = 
\begin{bmatrix}
\frac{\delta C_0}{\delta w^{(1)}}
\\
\frac{\delta C_0}{\delta b^{(1)}}
\\
\frac{\delta C_1}{\delta w^{(1)}}
\\
\frac{\delta C_1}{\delta b^{(1)}}
\\
\vdots
\\
\frac{\delta C_n}{\delta w^{(1)}}
\\
\frac{\delta C_n}{\delta b^{(1)}}
\end{bmatrix}
$$
This equations apply to a single neuron per layer system, to add more neurons just use:
$w_{jk}^{(L)}$ as the neuron connecting the neuron $j$ of the layer $L-1$ to the neuron $k$ of the layer $L$.
$a_j^{(L)}$ as the neuron $j$ in the layer $L$.

So now:

$$
C_o = \sum_{j=0}^{nL-1}(a_j^{(L)} - y_j)^2
$$
And:
$$
\frac{\delta C_0}{\delta a_k^{(L-1)}} =
\sum^{nL-1}{
    \frac{\delta z_j^{(L)}}{\delta a_k^{(L)}}
    \frac{\delta a_j^{(L)}}{\delta z_j^{(L)}}
    \frac{\delta C_0}{\delta a_j^{(L)}}
}
$$
