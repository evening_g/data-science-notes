It's a variation of [[Recurrent Neural Networks]] that aims to fix the problem of the gradient. Each neuron has a memory cell and 3 gates: input, output and reset. 

**Input:** Receives the state from other neurons (not all of them).
**Output:** Propagates its activation state to other neurons (not all of them). 
**Reset:** Makes the neuron "forget" its previous state.

This neural network has the capacity to learn complex sequences, like music or text.

[Long Short-Term Memory Paper](https://www.bioinf.jku.at/publications/older/2604.pdf).

![[lstm.png]]
