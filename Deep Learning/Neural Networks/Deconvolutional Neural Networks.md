These are "inverted" convolutional networks, these can be used to **generate images** from a given category, etc. Each layer grows by a given factor, usually 2.

![[dn.png]]

[Deconvolutional Networks](https://www.matthewzeiler.com/mattzeiler/deconvolutionalnetworks.pdf)
