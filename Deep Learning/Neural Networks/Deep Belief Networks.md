This includes most stacked architectures, like [[Restricted Boltzmann Machines]] or [[Variational Autoencoders|Variational Autoencoders]].

It is demonstrated that these networks can be trained stack by stack, where each stack only needs to learn the previous stack. This is also known as a **greedy training**. They can be trained by [[Backpropagation]] or [[Contrastive Divergence]].

After trained by [[Unsupervised Learning]], it can be used to generate new data, or if trained by **Contrastive Divergence** it can be used to classify existing data.

Before **DBN**, deep networks (models with hundreds of layers) were considered too hard to train. But later the **DBM** got a better result in the MNIST.

**DBM**Also demonstrated that it is better to pretrain a model using [[Boltzmann Machines|BM]] or [[Autoencoders]] to obtain the weights instead of generating them randomly.

![[dbn.png]]

[Greedy Layer-Wise Training of Deep Networks](https://proceedings.neurips.cc/paper_files/paper/2006/file/5da713a690c067105aeb2fae32403405-Paper.pdf)

