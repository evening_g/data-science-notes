Another [[Autoencoders|autoencoder]] but it is also fed with noise no avoid the NN learning small details and focusing on bigger characteristics.

![[dae.png]]

[Extracting and Composing Robust Features with Denoising Autoencoders](http://machinelearning.org/archive/icml2008/papers/592.pdf)