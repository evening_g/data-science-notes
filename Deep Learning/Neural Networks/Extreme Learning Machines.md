They're similar to [[Multilayer Perceptron|FFNN]] but with random connections. They use [[Minimum Square Error]] as training. This shows worse results but it's fast.

![[elm.png]]

[Extreme Learning Machine: Theory and applications](https://www.sciencedirect.com/science/article/abs/pii/S0925231206000385?via%3Dihub)

