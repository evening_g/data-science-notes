These are kinda the opposite of [[Autoencoders]]. The center layers are wider than the input and output layers. These can be used to extract small characteristics.

Instead of only feeding-forward the input, we add a "dispersion control", that sets a threshold so not all neurons are activated. In this way, it's similar to the neurons with peaks.

![[sae.png]]

[Efficient Learning of Sparse Representations with an Energy-Based Model](https://proceedings.neurips.cc/paper_files/paper/2006/file/87f4d79e36d68c3031ccf6c55e9bbd39-Paper.pdf).
