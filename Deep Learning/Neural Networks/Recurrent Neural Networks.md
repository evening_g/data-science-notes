Neurons receive information not only from the previous layer but also from the current layer but in the previous state. Therefore the order of the input affects the output. They are used to auto completion tasks, because it can predict the next step based on the previous.

A great problem with these networks is that the gradient tends to go to 0 or infinite.

![[rnn.png]]
See: *Elman, Jeffrey L. "Finding structure in time." Cognitive science 14.2 (1990): 179-211*.