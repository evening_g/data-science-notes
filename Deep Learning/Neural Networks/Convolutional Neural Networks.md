This is a paradigm shift in neural networks. They're mainly used for **image processing** but can be also used for other things, like **audio**. A typical use of these networks is classifying images. Sometimes a [[Multilayer Perceptron|FFNN]] is appended at the end to allow higher abstraction.

In these networks, neurons are not connected to all neurons in the previous layer, only to a few of them. Layers tend to become smaller, usually divided by a factor of 2.

![[cnn.png]]

[Gradient-Based Learning Applied to Document recognition](http://yann.lecun.com/exdb/publis/pdf/lecun-98.pdf)
