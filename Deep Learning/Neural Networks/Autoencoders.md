They're similar to a [[Multilayer Perceptron|Feedforward Neural Network]]. The networks has the shape of a sand clock and are **symmetrical**, the weights and bias can also be symmetrical. In general, an autoencoder is a encoder + bottleneck + decoder. They're a [[Unsupervised Learning]] algorithm.

The bottleneck is the smallest layer in the net, and it represents the [[Latent Space]] of the data, which is a representation that only focuses in the most important features. To make this possible, **data should have dependencies across dimensions**.

Then, making use of the latent space, we can use these networks for [[Dimensionality Reduction]], this offers more advantages than the traditional [[Dimensionality Reduction#PCA|PCA]] like learning to **encode non-linear dependencies** (when using non-linear [[Activation Functions]]) with the trade-off of a slower training time. For training a network with this purpose, a decoder is attached at the end of the network, this forming an autoencoder. The training process consists in encoding the data, decoding it again and matching it with the original data.

Autoencoders are trained via [[Backpropagation]], where we aim to minimize the [[Evaluation Metrics#Reconstruction error|reconstruction error]]. We do not want to overfit the model, because then we couldn't use it to create new data, we can do this by adding a *regularization* term to the error. Other solutions evolve [[Variational Autoencoders]] and [[Denoising Autoencoders]].

## Dimensionality Reduction

After training, we can use the encoding part for reducing dimensions, as it contains the **latent space** of the data we input.

![[Autoencoder vs PCA.png]]
> Image showing the difference between reducing from 2 to 1 dimension using an Autoencoder vs using PCA.

## Generating data

For generating new data, we take the **latent space** and make a few changes, hopefully, it will be decoded into something that makes sense.

Their ability to reduce non-linear data makes them useful also for [[Interpolation]].

## Denoising

This has interesting applications, like reconstructing audio. For this application, we have a original data, a "noisy" data. The autoencoder is fed with the noisy data. The [[Evaluation Metrics#Reconstruction error|reconstruction error]] is taken between the original data and the reconstructed data. The **latent space** is supposed to contain the important characteristics of the data while **excluding the noise**.

## Anomaly Detection

TODO
 
---

![[ae.png]]

**To learn more**

[Bourlard, H., & Kamp, Y. (1988). Auto-association by multilayer perceptrons and singular value decomposition. _Biological Cybernetics, 59_, 291-294.](https://www.semanticscholar.org/paper/Auto-association-by-multilayer-perceptrons-and-Bourlard-Kamp/f5821548720901c89b3b7481f7500d7cd64e99bd?p2df).
[Valerio Velardo - The Sound of AI, Autoencoders Explained Easily](https://youtu.be/xwrzh4e8DLs?si=mJENGIp0V5Td5hRk)

