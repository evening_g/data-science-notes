This is the function [Artificial Neural Networks](Artificial%20Neural%20Networks.md) need to standardize the values in each neuron. Without it, the ANN would only be able to learn linear systems.

See [How Activation Function Work in Deep Learning](https://www.kdnuggets.com/2022/06/activation-functions-work-deep-learning.html).

## Sigmoid

This was used in the early ANNs. But it can be used in [binary classification](Binary%20Classification.md) because the output can be interpreted as probability.

$$
\sigma(x) = \frac{1}{1 + e^{-x}}
$$

![Sigmoid Plot](Sigmoid%20Plot.png)
## ReLU

Rectified Linear Unit. Has a better performance and avoids the [vanishing gradients](Vanishing%20Gradients.md) problem. Also it's supposed to have a more similar function than biologic neurons.

$$
ReLU(x) = \max(x, 0) = \frac{x+|x|}{2}
$$
![ReLU plot](ReLU%20Plot.png)

## Leaky ReLu

TODO

## Hyperbolic tangent

It's similar than the sigmoid, but maps the numbers from $-1$ to $1$.

![Hyperbolic tangent Plot](Hyperbolic%20tangent%20Plot.png)

## Threshold function

if $x > \text{threshold}$ return $1$, else $0$? TODO.
