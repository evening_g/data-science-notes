These are similar to [[Autoencoders]] but they know a distribution of the approximated probability of the input samples. They're based in Bayesian maths to identify the neurons that affect the result.

In general, variational autoencoders to **solve issues with vanilla autoencoders**: the representation of characteristics in the [[Latent Space|latent space]] is not consistent, which means some characteristics are underrepresented and there are discontinuities.

The main differences between variational autoencoders and vanilla ones is the **encoder** and the **loss function**. This aims to make the latent space follow a **multivariable [[Normal Distribution|Gaussian Distribution]]**.

**VAE requires all dimensions to be independent**, ([[Pearson Correlation|ρ]] = 0), the [[Covariance Matrix]] results in a diagonal matrix (a matrix filled with 0's but in its diagonal).

Sampling the points through the normal distro, we can almost ensure that the **latent space** has no empty spaces and all characteristics are equally represented. If we sample any point in the latent space, it will make sense and there's more possibility that the decoder creates something that has sense.

![[vae.png]]

[Auto-Encoding Variational Bayes](https://arxiv.org/pdf/1312.6114v10.pdf)
[Valerio Velardo - The Sound of AI, From Autoencoders to Variational Autoencoders: Improving the Encoder](https://youtu.be/b8AzCgY1gZI?si=TTa60bMdKM9DoRss)
