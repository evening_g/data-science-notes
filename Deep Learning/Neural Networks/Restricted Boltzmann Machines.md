These are very similar to [[Boltzmann Machines]], but they don't have every neuron connected. They work in "groups" instead of layers. Input/output neurons are not interconnected, same for hidden neurons. Data is propagated forwards and backwards until converging.

![[rbm.png]]

[Paul Smolensky, Information Processing in Dynamical Systems: Foundations of Harmony Theory](https://apps.dtic.mil/sti/tr/pdf/ADA620727.pdf)
