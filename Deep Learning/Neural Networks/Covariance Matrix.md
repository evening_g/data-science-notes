This is a square matrix that measures the [[Covariance]] between each pair of variables. It is similar to the [[Correlation Matrix]] but it isn't scaled.

It is usually denoted as $\Sigma$.