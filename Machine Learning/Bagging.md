This is a set model. It is usually better because it creates a set of models, each model was trained with a different set of data.

[Random Forest](Random%20Forest.md) falls in this category.

In scikit-sklearn there us a Bagging Classifier and a Bagging Regressor.

## Bagging Regressor

```python
from sklearn.ensemble import BaggingRegressor

# N is the number of models in the set
# You should be careful as you don't want to kill your computer
bagging_model = BaggingRegressor(regressor_model, n_estimators=N)

test_preds_grid = bagging_model.fit(x_train_pca, y_train)
```

https://scikit-learn.org/stable/modules/generated/sklearn.ensemble.BaggingRegressor.html

## Bagging Classifier

```python
from sklearn.ensemble import BaggingClassifier

bagging_model = BaggingClassifier(classifier_model,
                                  n_estimators=,
                                  max_samples=,
                                  max_features=)
```

`n_estimators` = Number of models in the set (int).
`max_samples` = Max amount of samples that will be used for the resampling (float).
`max_features` = Max amount of features that will be taken to train each base estimator (float or int)

https://scikit-learn.org/stable/modules/generated/sklearn.ensemble.BaggingClassifier.html
