Linear regression is often a bad idea, however it might be useful when we have a high [correlation](Pearson%20Correlation.md).

It is included by default in R.

```R
model <- lm(y ~ x0 + x1 + ...)
```

You can of course change the operators, but `+` is always recommended.

Find the model summary.

```R
summary(model)
```

