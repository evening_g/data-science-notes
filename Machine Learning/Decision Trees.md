## Pros

- Easy to understand.
- Easy to represent graphically.
- They can use both numeric and categorical data.
- It is not necessary that the data have certain distribution.
- It requires much less cleaning and preprocessing. For example, it doesn't require standardization.
- It is not affected y outliers.
- Allow to easily find the most important variables.
- Can be applied to regression and classification problems.

## Cons

- They tend to overfit. However it is easily fixed using [bagging](Bagging.md), [random forest](Random%20Forest.md) and [boosting](Boosting.md).
- They can be easily disbalanced.
- They might broke if a data out of the test data range is presented **(extrapolation).** Usually, if a data out of the test data range is presented, it returns the value of the nearest node, no matter how far the data is.

## When to use

Decision trees can be very powerful when applied in the right data. Take these images as example.

![Decision tree lineal](Decision%20tree%20lineal.png)
![Decision tree no lineal](Decision%20tree%20no%20lineal.png)
As you see. If the data distribution is linear, it is better to create a linear regression model.

Trees can be also used to find the most important variables in a model.

## Avoid Overfitting

As mentioned previously, decisions trees tend to overfit. There are two strategies we can take to avoid this.

### Early stopping

We can add rules to control the final size of the tree. There are many rules we can add we must adjust these rules to get a flexible model while avoiding overfitting.. These are the most common.

- **Minimum observations for splitting:** The nodes are not split until it has the minimum amount. This avoids creating a a log of nodes with single values. However **the bigger the value, the less flexible the model.**
- **Minimum observations for leaves:** Defines the minimum number that leaf nodes must have. **the bigger the value, the less flexible the model.**
- **Tree maximum height:** This gives a better control on the tree's size. The smallest the value, the less flexible the model.
- **Tree maximum number of leaves:** It's very similar that above's.
- **Minimum reduction error:** Defines the minimum reduction error necessary to split a node.

### Pruning

As it says. We can remove some nodes after creating the model. Early stopping the model comes with the problem that the rules are evaluated when the tree is being created, so we unknown the data that are to come. Pruning solves this issue. There are many options to prune.

#### Cross-validation pruning

Uses a lot of resources so it's a **bad idea.**

#### Cost complexity pruning

**Cost complexity pruning:** We search for the tree that minimizes the equation:
$$
\sum^{|T|}_{j=1}\sum_{i\in R_j}(y_i - ŷR_j)^2 + \alpha|T|
$$
**Where:**

$|T|$ is the number of leaves.
The first term is the sum of the RSS.
The second term is the penalization (for having too much nodes).
$\alpha$ is the penalization factor. Can be determined using **k-cross-validation (?).**

Here an example of finding the best $\alpha$ using [Model Selection](Model%20Selection.md).

```python
import numpy as np
from sklearn.model_selection import GridSearchCV

params = {
	# Alpha value
    'ccp_alpha' :np.linspace(0,80,20)
}

grid = GridSearchCV(
	estimator = DecisionTreeRegressor(
		max_depth			= None,
		min_samples_split	= 2,
		min_samples_leaf	= 1,
		random_state		= 123
	),
	param_grid			= params,
	cv					= 10,
    refit				= True,
    return_train_score	= True
)

grid.fit(x_train, y_train)

ccp_alpha = grid.best_params_['ccp_alpha']

model = grid.best_estimator_
```

#### Weakest link pruning

## Usage

https://scikit-learn.org/stable/modules/classes.html#module-sklearn.tree

### Decision Tree Regressor

Regression trees divide the sample in rectangular regions, such that the division minimizes the **Residual Sum of Squares (RSS)**

$$
RSS = \sum^j_{j=1}\sum_{i\in R_j}(y_i-ŷR_j)^2
$$
Where $ŷR_j$ is the mean of the region $R_j$.

It however cannot try all possibilities. So it uses **Recursive Binary Division**.

```python
from sklearn.tree import DecisionTreeRegressor
```

Here you can watch all the parameters `DecisionTreeRegressor` has and what are their defaults.

https://scikit-learn.org/stable/modules/generated/sklearn.tree.DecisionTreeRegressor.html#sklearn.tree.DecisionTreeRegressor

```python
model = DecisionTreeRegressor(
	# Rules
)

model.fit(x_train, y_train)
```

### Decision Tree Classifier

Classification trees cannot use RSS, as the values are not numeric. There are different alternatives they can use.

In the following formulas. $\hat{P}_{mk}$ is the proportion of observations in the $m$ node that belong to the class $k$.

- **Classification Error Rate**

  It is the proportion of observations that do not belong to the majority class of the node.
  $$
  E_m = 1 - max_k(\hat{P}_{mk})
  $$
  This is not efficient and not commonly used.

- **Gini Index**

  It measures the node's purity. The smallest the index, the less pure.
  $$
  G_m = \sum^{K}_{k=1}\hat{P}_{mk}(1 - \hat{P}_{mk})
  $$
  [CART Algorithm](https://www.geeksforgeeks.org/cart-classification-and-regression-tree-in-machine-learning/) uses this metric.

- **Crossed Entropy**

  The smallest the entropy, the more pure the node. The values range from 0 to 1.
  $$
  D = - \sum^{K}_{k=1}\hat{P}_{mk}\log{(\hat{P}_{mk})}
  $$
  It is used by [C4.5 and C5.0 algorithms](https://en.wikipedia.org/wiki/C4.5_algorithm).

- [**Chi-Square**](Distance%20Metrics.md#Chi-Square)

  The biggest the value, the more impure the node. Trees generated using this metric are called **CHAID (Chi-Squared Automatic Interaction Detector).**

```python
from sklearn.tree import DecisionTreeClassifier
```

Here you can watch all the parameters `DecisionTreeClassifier` has and what are their defaults.

https://scikit-learn.org/stable/modules/generated/sklearn.tree.DecisionTreeClassifier.html#sklearn.tree.DecisionTreeClassifier

```python
# CRITERION: See the above metrics criterions for classifier trees
model = DecisionTreeClassifier(
	criterion = CRITERION
)

model.fit(x_train, y_train)
```

scikit-learn tree only supports pruning.

### Checking the tree's structure

```python
tree_depth = model.get_depth()
leaves = model.get_n_leaves()
```

### Graphic the tree

(Just for fun)

```python
from sklearn.tree import plot_tree

plot_tree(decision_tree = model)
```

### Export the tree

```python
_, ax = plt.subplots(figsize=(120,25))

sktree.plot_tree(
    decision_tree=model,
    feature_names=x_test.columns,
    filled=True,
    ax=ax
)
plt.savefig('tree.jpg',format='jpg',bbox_inches = "tight")
```

### Finding the most important features

```python
import pandas as pd

importances = pd.DataFrame(
   { 'predictor' : df.columns,
	 'importance': modelo.feature_importances_ }
)

importances.sort_values('importance', ascending=False, inplace=True)
```

### Printing the tree in text format

This is useful because you can then use it in other languages nicer than Python.

```python
from sklearn.tree import export_text

export_text(model)
```