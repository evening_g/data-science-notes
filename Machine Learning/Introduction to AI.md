If you don't know statistics please return later, or suffer as I did.

## Knowledge Pyramid

1. Data
2. Information (Organized data)
3. Knowledge (Meaningful information)
4. Wisdom (Ability to think, understand and create new knowledge)

## Knowledge Representation and Reasoning

It is not related to information storage, rather to how to make computers relate data to each other. It is important to use the adequate structure and format to avoid ambiguity.

### Triplets (Name, Attribute, Value)

|Name|Attribute|Value|
|---|---|---|
|House|Color|Yellow|
|House|Stories|2|
|House|Basement|Yes|

### Semantic networks

They're represented with a directed graph. The Google search engine uses a semantic network.

> Based on this network. We can generate knowledge, like Zacahuil is a food from América.

**Semantic distance**: Is the distance between one node and another.

### Production rules

They're represented with rules with the format _Condition \to Action_.

### Frames

It is a collection of attributes (slots), with values and restrictions. Sometimes the slots can contain another frame inside. You can think of a frame as a json.

```json
{  
    country: "México",  
    states: {  
        [  
            {state: "SLP"},  
            {state: "Nuevo León"},  
            ...  
        ]  
    }  
}
```

Frames can have triggers, modularity, inheritance.

### Scripts

It is like as a theater script. Where steps go after other step and conditions must be true for these to be executed.


## Types of attributes

- **Numerical:** Discrete or real.
  
- **Categorical:** These are labels that represent a value domain. For example weather.
  
- **Ordinal:** These can be sorted and can be either numerical or categorical. For example "hot", "warm", "cold".
  
- **Binary:** Have two possible values.