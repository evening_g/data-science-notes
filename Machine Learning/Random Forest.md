This is a [Bagging](Bagging.md) technique. Here, many trees are created on different parts of the data. After they're trained, we use the median of all the answers.

## Pros

- Bias is reduced and it's **hard to overfit**.

## Cons

- They require a lot of resources.

## Usage

There is, as usual, a [classification algorithm](Supervised%20learning.md#Classification%20algorithms) and a [regression algorithm](Supervised%20learning.md#Regression%20algorithms). Available in [scikit-learn](Must%20Have%20Libraries%20(Python).md#scikit-learn).

https://scikit-learn.org/stable/modules/generated/sklearn.ensemble.RandomForestClassifier.html

https://scikit-learn.org/stable/modules/generated/sklearn.ensemble.RandomForestRegressor.html

```python
from sklearn.ensemble import RandomForestClassifier

model = RandomForestClassifier(
    n_estimators = N,
    ...
)
```
It the same parameters than other [Decision Trees](Decision%20Trees.md).