It is a machine learning paradigm. In this paradigm, data consists of labeled data, which is split in training data and test data.

The **goal** of SL algorithms is to produce a function that given an input, produces the desired output. In an optimal scenario, the function is able to correctly determine unseen instances. However, this is not usually the case, so there are ways to determine the quality of a function.

SL algorithms can divided in classification and regression algorithms according to the type of the output. See [Types of attributes](Introduction%20to%20AI.md#Types%20of%20attributes).

For more information see: https://en.wikipedia.org/wiki/Supervised_learning

## General use algorithms

These algorithms are both for classification and regression.

- [K-Nearest Neighbors](K-Nearest%20Neighbors.md).
- [Decision Trees](Decision%20Trees.md)

## Classification algorithms

This is a prediction task where the output is a categorical value.

## Regression algorithms

In this algorithms, the output has a numerical value.

## Supervised learning process

Usually, when you're building a SL model, there are some steps you want to follow.

### 1. Understanding the data.

Here you have to understand what's your data about. Find out all about the data you are working with.

**Tips:**

- Search information on the web about the topic.
- The function `describe()` shows a lot of important data.
- Making plots of your target variable is very helpful, specially [histograms](Histogram.md).
- You can also make plots of the rest of the variables. This will be helpful to understand how data in general behaves.
- Make a [correlation matrix](Correlation%20Matrix.md). The most important correlation are the ones with the target variable. Remove the columns with high correlation.
- Remove the variables that you know, won't affect the model. However, be careful!, some variables might be tricky, if you don't feel sure you can wait for PCA.

### 2. Splitting the data

Now we have to split the data. We usually assign `x` to the rest of the variables and `y` to the target variable.

```python
x = df.drop('target', axis="columns")
y = df['target']
```

We also must split the data in training and test data.

```python
from sklearn.model_selection import train_test_split

# TestSize is the part of the data that we are going to take for test.
# 0 < TestSize < 1. Recommended between 0.7 and 0.2
# RandomState is a seed. It is optional but it's recommended so we can
# reproduce the same results again.

x_train, x_test, y_train, y_test = train_test_split(x, y,
    test_size = TestSize, # or train_size
    random_state = RandomState
    )
```

https://scikit-learn.org/stable/modules/generated/sklearn.model_selection.train_test_split.html

Check if the test and train data are similar.  Use `.describe()`.

### 3. Scaling the data

Data values usually have different domains between columns. Columns that use to have higher values would be taken are having more importance by the model, and columns with lower values would have less importance. So it is important to scale the `x` data.

`sklearn.preprocessing.MinMaxScaler` scales all the data to have values between 3 and -3.

```python
from sklearn.preprocessing import MinMaxScaler

scaler = MinMaxScaler()
x_train = scaler.fit_transform(x_train)
x_test  = scaler.fit_transform(x_test)
```

https://scikit-learn.org/stable/modules/generated/sklearn.preprocessing.MinMaxScaler.html

### 4. Dimensionality Reduction

The more variables we have, the more slow our model will be. PCA helps us to identify what variables have a low impact on the model so we can remove them.

```python
from sklearn.decomposition import PCA

# NumberOfComponents: It is bettwe to check `Dimensionality Reduction` for this

pca = PCA(n_components = NumberOfComponents)

pca.explained_variance_ratio_

```

There is a whole document dedicated to this topic. See [Dimensionality Reduction](Dimensionality%20Reduction.md).

### 5. Creating the model

We've arrived to the important part of the section. Here we're ready to create our model. Please refer to the beginning of the section to find out what model adapts better to your needs.

So, most models work this way:

```python
# Initialize model
# ...

# Train the model
model.fit(x_train, y_train)

# Get the test predictions
predictions = model.predict(x_test)
```



### 6. Model Evaluation

There are many [metrics for evaluating regression models](Evaluation%20Metrics.md).

### 7. Improving the model

We can try different parameters on the model and then evaluate it.

#### [Model Selection](Model%20Selection.md)

`GridSearchCV` uses exhaustive search to find the best parameters for the model.

#### Bagging

This is a set model. It is usually better because it creates a set of models. Please refer to [Bagging](Bagging.md) for more information.

#### Distance metrics

There are different distance metrics to know the distance between to points. You should check out the following document to help you find out what is the best metric for your model. [Distance Metrics](Distance%20Metrics.md).