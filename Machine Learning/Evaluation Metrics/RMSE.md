The **Root-Mean-Square Deviation** measures the average difference between a statistical mode's predicted values and the actual values. It must be squared because otherwise, positive and negative values would cancel each other.

![](RMSE.png)

```python
from sklearn.metrics import mean_squared_error

rmse = sqrt(mean_squared_error(y_test, predictions))
```

The highest the value, the worse the model.