For all the code in this section, consider the following:

```python
# Find the test predictions
predictions = model.predict(x_test)
```

I've also created this function to show different metrics.

```python
import sklearn.metrics as skmetrics
from math import sqrt

def show_metrics(y_test, predictions):
    r2s = skmetrics.r2_score(y_test, predictions)
    mae = skmetrics.mean_absolute_error(y_test, predictions)
    mse = skmetrics.mean_squared_error(y_test, predictions)
    rmse = sqrt(mse)
    
    print(f"R2 Score = {r2s}")
    print(f"     MAR = {mae}")
    print(f"     MSE = {mse}")
    print(f"    RMSE = {rmse}")

```

## [RMSE](RMSE.md)

```python
from sklearn.metrics import mean_squared_error

rmse = sqrt(mean_squared_error(y_test, predictions))
```

## [R Squared Score](R%20Squared%20Score.md)

```python
from sklearn.metrics import r2_score

r2_score = r2_score(y_test, predictions)
```

## [MAE](Mean%20Absolute%20Error%20(MAE).md)

```python
from sklearn.metrics import mean_absolute_error

mae = mean_absolute_error(y_test, predictions)
```

## [MSE](Mean%20Squared%20Error%20(MSE).md)

```python
from sklearn.metrics import mean_squared_error

mse = mean_squared_error(y_test, predictions)
```

## [Crossed Validation Score](Crossed%20Validation%20Score.md)

```python
from sklearn.model_selection import cross_val_score

scores = cross_val_score(
	estimator = model,
	x = x_train,
	y = y_train
)
```

## [Confusion Matrix](Confusion%20Matrix.md)

```python
from sklearn.metrics import confusion_matrix

matrix = confusion_matrix(y_test, predictions)

# or into a heatmap
import seaborn as sns
sns.heatmap(
            confusion_matrix(y_test, predictions),
            annot=True,
            fmt=".0f")
```

## [Classification Report](Classification%20Report.md)

```python
from sklearn.metrics import classification_report

report = classification_report(predictions, y_test)
```

## Reconstruction error

It's the difference between the original data and the reconstructed data. It's used mainly to train [[Unsupervised Learning]] models.
