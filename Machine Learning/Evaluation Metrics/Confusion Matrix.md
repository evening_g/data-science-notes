https://en.wikipedia.org/wiki/Confusion_matrix

This is only used for [Classification algorithms](Supervised%20learning.md#Classification%20algorithms). It is read as follows:

![Confusion Matrix](Confusion%20Matrix.png)



Available in [scikit-learn](Must%20Have%20Libraries%20(Python).md#scikit-learn).

https://scikit-learn.org/stable/modules/generated/sklearn.metrics.confusion_matrix.html

```python
from sklearn.metrics import confusion_matrix

matrix = confusion_matrix(y_test, predictions)
```

There is a fancy way to display it.

https://scikit-learn.org/stable/modules/generated/sklearn.metrics.ConfusionMatrixDisplay.html#sklearn.metrics.ConfusionMatrixDisplay

```python
from sklearn.metrics import confusion_matrix, ConfusionMatrixDisplay

matrix = confusion_matrix(y_test, predictions)

display = ConfusionMatrixDisplay(confusion_matrix=matrix)
display.plot()
```

