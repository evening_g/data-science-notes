https://en.wikipedia.org/wiki/Mean_squared_error

**Mean Squared Error (MSE)** is the average of the square of the errors. In this case, the error is the difference between the observed and the predicted values. We square each difference so that negative and positive don't cancel each other.

If $\text{MSE} = 0$ then the model is perfect.

It is available in [scikit-learn](Must%20Have%20Libraries%20(Python).md#scikit-learn).

```python
from sklearn.metrics import mean_squared_error

mse = mean_squared_error(y_test, predictions)
```