https://en.wikipedia.org/wiki/Coefficient_of_determination

Also **coefficient of determination**, denoted $R^2$. is the proportion of the variance in the dependent variable that is predictable from the independent variables. For example, if we have $R^2 = 100\%$, then all of our $y$ can be predicted from $x$. It is also a way to determine [Correlation](Vocabulary.md#Correlation).

We can find it using [scikit-learn](Must%20Have%20Libraries%20(Python).md#scikit-learn).

https://scikit-learn.org/stable/modules/generated/sklearn.metrics.r2_score.html

```python
from sklearn.metrics import r2_score

r2_score = r2_score(y_test, predictions)
```