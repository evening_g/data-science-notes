https://scikit-learn.org/stable/modules/generated/sklearn.metrics.mean_squared_error.html

**Mean Absolute Error (MAE)** is very similar to [MSE](Mean%20Squared%20Error%20(MSE).md), but instead of using squares to avoid positive and negative cancel each other, we use the absolute value. MAE is calculated as the sum of absolute errors divided by the sample size.

It is also available in [scikit-learn](Must%20Have%20Libraries%20(Python).md#scikit-learn).

https://scikit-learn.org/stable/modules/generated/sklearn.metrics.mean_absolute_error.html

```python
from sklearn.metrics import mean_absolute_error

mae = mean_absolute_error(y_test, predictions)
```