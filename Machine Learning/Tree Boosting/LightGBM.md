```python
pip install lightgbm
```

https://lightgbm.readthedocs.io/en/stable/

## Usage

```python
from lightgdm import LGBMRegressor

model = LGBMRegressor(random_state=SEED)

model.fit(x_train, y_train)
```