This is a generalization of [AdaBoost](AdaBoost.md). It is **very flexible** and is used for [classification](Supervised%20learning.md#Classification%20algorithms) and [regression](Supervised%20learning.md#Regression%20algorithms).

In this technique, many models are trained sequentially. Each new model tries to fix the erros of the previous.

It is very susceptible to overfitting.

## Usage

We can use the [scikit-learn](Must%20Have%20Libraries%20(Python).md#scikit-learn) `GradientBoostingRegressor`. There's also a function for classification models.

https://scikit-learn.org/stable/modules/generated/sklearn.ensemble.GradientBoostingRegressor.html

```python
from sklearn.ensemble import GradientBoostingRegressor

model = GradientBoostingRegressor(
	n_estimators = N,
	max_features = M,
	random_state = SEEED
)

model.fit(x_train, y_train)
```

See [Decision Trees / Usage](Decision%20Trees.md#Usage) for more information about other parameters.

- **learning_rate:** Determines the contribution of each tree.
- **subsample:** Proportion of the data used for each tree. If you set a value smaller to 1 you're using [Stochastic Gradient Boosting](Stochastic%20Gradient%20Boosting.md).
- **validation_fraction:** Proportion of the data used as validation set to determine the early stopping.
- **n_iter_no_change:** Maximum number of iterations without changing before stopping the model.
- **tol:** Minimum improvement percentage between each iterations. If the value is lower, the model is considered as not improved.


