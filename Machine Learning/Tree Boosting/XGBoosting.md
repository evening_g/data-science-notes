```python
pip install xgboost
```

https://xgboost.readthedocs.io/en/stable/
https://www.nvidia.com/en-us/glossary/data-science/xgboost/

This is yet another implementation of the [Boosting](Boosting.md).

## Usage

```python
from xgboost import XGBRegressor

model = XGBRegressor(
	n_estimators = N
)

model.fit(X = y_train, Y = y_train)
```