This is a technique where trees are grouped. Boosting trees can return many results. Boosting trees try to fix the errors that normal trees have, however, it can suffer overfitting.

The main difference between Boosting and [Bagging](Bagging.md) is that Boosting trains the models sequentially, while bagging creates a set of models, and returns the average result among them.

There are many types of boosting.

- [Gradient Boosting](Gradient%20Boosting.md).
- [AdaBoost](AdaBoost.md).
- [Stochastic Gradient Boosting](Stochastic%20Gradient%20Boosting.md).
- [Light Boosting](LightGBM.md).
- [XGBoosting](XGBoosting.md).
- H20

All these models have a lot of hyperparameters.

- **Weak learners:** If too high causes overfitting, we often use a value called **learning rate**.
- **Learning rate:** Controls the influence of each weak learner in the final set. The recommended values are between 0.001 and 0.01. The lower the value, the more trees are needed, and the lower the risk of overfitting.

## Comparison between boosting algorithms

|                     | Gradient Boosting      | AdaBoost | XGBoost  | CatBoost | LightGBM | H2O      |
| ------------------- | ---------------------- | -------- | -------- | -------- | -------- | -------- |
| Categorical data    | Requires preprocessing | No       | No       | Yes      | No       | Yes      |
| Speed               | Moderate               | Fast     | Fast     | Moderate | Fast     | Fast     |
| Memory usage        | Moderate               | Low      | Moderate | High     | Low      | Moderate |
| Parallel processing | No                     | No       | Yes      | Yes      | Yes      | Yes      |
| GPU support         | No                     | No       | Yes      | Yes      | Yes      | Yes      |
| Allows binning      | No                     | No       | Yes      | No       | Yes      | Yes      |
| Allows missing data | No                     |          | Yes      | Yes      | Yes      | Yes      |

