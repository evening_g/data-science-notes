KNN is a [Supervised learning](Supervised%20learning.md) for both [Classification algorithms](Supervised%20learning.md#Classification%20algorithms) and [Regression algorithms](Supervised%20learning.md#Regression%20algorithms).

## Pros

- KNN is very fast.
- KNN is easy to understand by humans, in contrast with neural networks.
- Works good when there is **missing data.**

## Cons

- It is not good when there are many variables.
- It isn't good for natural language processing or computing vision.
- It has in general, worse performance than neural networks.
- It uses a lot of resources.

## K Neighbors Regressor

This is the regressor version of the algorithm.

https://scikit-learn.org/stable/modules/generated/sklearn.neighbors.KNeighborsRegressor.html

It is available in:

```python
from sklearn.neighbors import KNeighborsRegressor
```

**Usage:**

```python
# nitialize with the number of neighbors we want
knn_model = KNeighborsRegressor(n_neighbors=3)

# Fit with the data
knn_model.fit(x_training, y_training)
```

## K Neighbors Classifier

This is the classifier version of the algorithm. Here you can learn about it:

https://scikit-learn.org/stable/modules/generated/sklearn.neighbors.KNeighborsClassifier.html#sklearn.neighbors.KNeighborsClassifier

```python
from sklearn.neighbors import KNeighborsClasiffier
```

```python
# Number of neighbors to take
knn_model = KNeighborsClasiffier(n_neighbors = N)

knn_model.fit(x_training, y_training)
```

## KNN Imputer

This algorithm helps us to impute missing data. The algorithm receives a $K$ number of neighbors. When it finds a `NaN` value, it finds the $K$ nearest neighbors and assigns their average to the miss

https://scikit-learn.org/stable/modules/generated/sklearn.impute.KNNImputer.html

```python
from sklearn.impute import KNNImputer
```

```python
# K = number of neighbors to take

imputer = KNNImputer(n_neighbors = K)
imputed = imputer.fit_transorm(df)

```

