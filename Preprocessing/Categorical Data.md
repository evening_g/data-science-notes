> See [Types of attributes](Introduction%20to%20AI.md#Types%20of%20attributes) if you have any doubts on what this means.

Most algorithms work with numerical data. So it is important to know what to do when we find categorical data.

## Parsing categorical to numerical

Take for example: "Bad", "Regular", "Good", "Excellent".

We might think that we can assign a number to each value:

```python
values: {
    "Bad": 1,
    "Regular": 2,
    "Good": 3,
    "Excellent": 4
}
```

But we might have problems with these, as this suggest that all categories are equidistant. So we must be careful and adapt the values to our data.

---

### Ordinal Encoder

https://scikit-learn.org/stable/modules/generated/sklearn.preprocessing.OrdinalEncoder.html

```python
from sklearn.preprocessing import OrdinalEncoder

encoder = OrdinalEncoder(categories: list()) -> OrdinalEncoder

```

We can put this in a dataframe:

```python
df = pd.DataFrame(encoder.fit_transform(df), columns=["columns", "to", "update"])
```

**Example**

```python
encoder = OrdinalEncoder(categories=[df['Sex'].unique()])  
  
df['Sex'] = encoder.fit_transform(df[['Sex']])
```

### One-Hot Encoder: Dummy Variables

This algorithm creates a column for each category. Therefore it's better for **not ordinal categories.**

The names of the columns are represented as an array containing the names of the categories it belongs (usually just one). So probably you want to change the name of the columns so it has strings instead of arrays.

https://scikit-learn.org/stable/modules/generated/sklearn.preprocessing.OneHotEncoder.html

```python
from sklearn.preprocessing import OneHotEncoder

# Note that this must be done per column
encoder = OneHotEncoder()

encoded = encoder.fit_transform(df[['column']])
new_columns = pd.DataFrame(encoded.toarray(), columns=encoder.categories_)

df = pd.concat([df, new_columns], axis="columns", ignore_index=True)

# Remove the brakets
cols = []  
  
for col in df.columns:  
    cols.append(str(col).replace("('","").replace("',)",""))  

df.columns = cols
```

## Parsing numerical attributes to categorical

```python
df['column'] = df['column'].astype("category")
```

