It is often convenient to remove outliers as these may affect statistics. We usually use a [Boxplot](Boxplot.md) to visualize the outliers.

We remove data above or below the whiskers.

```python
# Find quantiles
q1 = df['column'].quantile(0.25)
q3 = df['column'].quantile(0.75)
# Inter-quantile range
iqr = q3 - q1

lower_whisker = q1 - 1.5 * iqr 
upper_whisker = q3 + 1.5 * iqr

# Remove outliers
outliers = df.loc[(df['column'] < lower_whisker) | (df['column'] > upper_whisker)].index
df.drop(outliers, inplace=True)
```