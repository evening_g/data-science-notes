## Remove columns with high cardinality

Cardinality is the amount of different values. This is only applied to **categorical data**, because numeric data. by its nature, always has high cardinality.

Having high cardinality can affect our model.

```python
categorical_columns = df.select_dtypes('object').columns  
  
cardinalities = pd.DataFrame({  
    "columns" : categorical_columns,  
    "cardinality" : [df[col].unique().size for col in categorical_columns]  
})  
  
cardinalities
```

## Remove columns with low variance

Variance refers on how far are the values from the median. See [Variance](Variance.md).

Columns with low variance don't provide different ranges of data.

```python
numeric_columns = df.select_dtypes('number').columns  
  
variances = pd.DataFrame({  
    'columns': numeric_columns,  
    'variance': [((df[col] - df[col].mean()) / df[col].std()).var() for col in numeric_columns]  
})  
  
variances
```

## Remove columns with high [correlation](Pearson%20Correlation.md)

Columns with high correlation contribute with the same information, so it is a good idea to leave only one of them to avoid complicating the model.

```python
corr = df.corr()
cols = corr.where(np.triu(np.ones(corr.shape), k=1).astype(np.bool_))  
cols_to_remove = [col for col in cols.columns if any(np.abs(cols[col]) > CORR_THRESHOLD)]  
cols_to_remove
```

The CORR_THRESHOLD is usually 0.7.