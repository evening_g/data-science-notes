Missing data is a big problem when analyzing data. There is no satisfactory way of fixing this, so it is important to strictly compile the data.

It is sometimes imposible to have totally clean data. And there are a few things we can do about. Some of the main options are:

- **Remove variables with missing data (columns).**

  If a column is missing a lot of data, maybe we should remove it.

- **Remove registers with missing data (rows).**

  If a row is missing a lot of data, we should remove it.

- **Replace the missing data with average data.**

  It is important to determine how are we going to replace the missing data. We cannot replace it by the general average in all the cases. We can use [KNN Imputer](K-Nearest%20Neighbors.md#KNN%20Imputer) to estimate missing data.

- **Replace the missing data with the mode.**

  The advantage of this is that it isn't affected by outliers.
```python
df['col'].fillna(df['col'].mode()[0], inplace=True)  
```

- **Interpolation.**

  TODO.
  
- **Select an algorithm that performs well with missing data.**

  There are some algorithms like [K-Nearest Neighbors](K-Nearest%20Neighbors.md) that work well with missing data.

## Finding missing data

```python
import pandas as pd

# ...

df.isnull().sum()
```
