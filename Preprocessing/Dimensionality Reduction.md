The more variables we have, the more complex the model is. It is important to remove variables that have low impact on the model. Keeping these variables could also cause [overfitting](Introduction%20to%20AI.md#Vocabulary#Overfitting).

There are many ways to reduce dimensionality.

## Removing dimensions

We must be sure that we're removing dimensions with low importance.

## Characteristics extraction

We create new characteristics based on the old ones. The new characteristics combine the old characteristics. There are tools to help us with this, such as [PCA](Dimensionality%20Reduction.md#PCA).

## PCA

The functioning of PCA is weird an interesting. If you're interested to know what happens backstage you can check this: https://en.wikipedia.org/wiki/Principal_component_analysis.

The main **disadvantage** of PCA is that it **only learns linear relationships**.

So, PCA analyzes the data and returns new dimensions, sorted from best to worst. Our job is to select the dimensions to use. There are different methods for doing this.

- Arbitrary choose the first n dimensions.
- Find the [explained variance proportion](Explained%20Variance%20Ratio.md) and select dimensions until you have a given minimum. **Usually 85%.**
- Use the [Elbow method](Elbow%20Method.md) to find out how many dimensions to take.

```python
from sklearn.decomposition import PCA

pca = PCA()
df_pca = pca.fit_transform(df)
```

The explained variance ratio can be found in:

```python
pca.explained_variance_ratio_
```

This is an array, where each element is the explained variance ratio of that dimension. Here is where you choose how many dimensions keep. You can drop the rest of the dimensions.

You can find more information for this method here:


https://scikit-learn.org/stable/modules/generated/sklearn.decomposition.PCA.html#sklearn.decomposition.PCA

## Dimensionality Reduction through neural networks

See [[Autoencoders]].