Is the ratio of the variance of estimating some parameter in a model that includes multiple other parameters by the variance of a model constructed using only one parameter.

https://en.wikipedia.org/wiki/Variance_inflation_factor

Usually we remove columns with the highest VIF until we reach a goal, usually 5.

```python
from statsmodels.stats.outliers_influence import variance_inflation_factor

while True:
    vif = pd.DataFrame()
    vif['VIF'] = [variance_inflation_factor(X.values, i) for i in range(X.shape[1])]
    vif['features'] = X.columns
    vif.round(1)
    max_index = vif['VIF'].idxmax()

    print(f"Removing {vif['features'][max_index]}")

    if vif['VIF'][max_index] >= 5:
        df.drop(vif['features'][max_index], axis='columns', inplace=True)
        X.drop(vif['features'][max_index], axis='columns', inplace=True)
    else:
        break
```