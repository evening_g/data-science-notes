This one is based on the [Taylor Polynomial](Taylor%20Polynomial.md). It requires $n$ to be a multiple of 3. This is more precise than [Simpson ⅓](Simpson%20⅓.md) and [Trapezoidal Rule](Trapezoidal%20Rule.md).

$$
I \approx \frac{3h}{8}
\left[
f(x_0) + f(x_n)
+
\begin{Bmatrix}
i \mod 3 = 0& 2 f(x_i)
\\
i \mod 3 = 1& 3 f(x_i)
\\
i \mod 3 = 2& 3 f(x_i)
\\
\end{Bmatrix}
\forall i \in 1..n-1
\right]
$$
```python
import numpy as np

def simpsom_three_eights(y: np.array, h, a, b) -> np.number:
    if (b-a) % 3 != 0:
        raise ValueError("Simpsom 3/8 requires a multiple of 3 number of points")

    return (3*h/8)*(y[a]+y[b]
                    + 2 * sum(y[i] for i in range(a+3, b, 3))
                    + 3 * sum(y[i] for i in range(a+1, b, 3))
                    + 3 * sum(y[i] for i in range(a+2, b, 3))
                    )
```
