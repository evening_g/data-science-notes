This one is based on the [Taylor Polynomial](Taylor%20Polynomial.md). It requires $n$ to be even. This is more precise than [Trapezoidal Rule](Trapezoidal%20Rule.md).

$$
I \approx \frac{h}{3}
\left[
f(x_0) + f(x_n)
+ 
\begin{Bmatrix}
i \text{ is even} & 2\sum f(x_i)\\
i \text{ is odd} & 4\sum f(x_i)
\end{Bmatrix}
\forall i \in 1..n-1
\right]
$$
```python
import numpy as np

def simpsom_one_third(y: np.array, h, a, b) -> np.number:
    if (b-a) % 2 != 0:
        raise ValueError("Simpsom 1/3 requires an even number of points")

    return (h/3)*(y[a]+y[b]
                  + 2 * sum(y[i] for i in range(a+2, b, 2))  # even numbers
                  + 4 * sum(y[i] for i in range(a+1, b, 2))  # odd numbers
                  )

```
