This method is used on tabular functions. It is based in making trapezoids using each adjacent pair of points and computing their area.

$$
I \approx \frac{h}{2}
\left[
	f(x_0) + f(x_n) +
	2 \sum_{i=1}^{n-1} f(x_i)
\right]
$$
```python
import numpy as np

def trapezoid(y: np.array, h, a, b) -> np.number:
    return (h/2)*(y[a] + 2*sum(y[a+1: b]) + y[b])
```