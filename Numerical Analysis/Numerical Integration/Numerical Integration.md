This methods are applied when we have tabular functions.

In the following formulas, use the following notation.

$n$ is the number of points - 1.
$h$ is the space between the points; $x_{i+1} - x_i$ each pair of points must have the same $h$.

- [Trapezoidal Rule](Trapezoidal%20Rule.md).
- [Simpson ⅓](Simpson%20⅓.md).
- [Simpson ⅓](Simpson%20⅓.md)