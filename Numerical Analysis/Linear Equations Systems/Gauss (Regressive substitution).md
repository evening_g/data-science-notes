Find the triangular matrix dividing the rows by the pivot and subtracting rows to each other. After the triangular is found, do regressive substitutions to find the values.

**Requirements**

There must not be zeroes in the diagonal.

```python
import numpy as np

def gauss(a: np.array, b: np.array) -> np.array:
	n = np.size(b)
	
	m = np.hstack((a, b.reshape(-1,1)))
	m = m.astype('float')
	
	for i in range(n):
		# make m[i][i] = 1
		if m[i][i] == 0:
			raise ValueError(f"zero division in m{i}{i}")

		m[i] = m[i] / m[i][i]

		# make zeros below diagonal
		for j in range(i+1, n):
			m[j] = m[j] - m[i]*m[j][i]

	x = np.zeros(n)

	for i in reversed(range(n)):
		x[i] = m[i][n] - sum(m[i][j]*x[j] for j in range(n))

	return x
```
