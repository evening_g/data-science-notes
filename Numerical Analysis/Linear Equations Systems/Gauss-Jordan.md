Make the diagonal values 1 and all the others zeros.

**Requirements**

There must not be zeros in the diagonal.

```python
import numpy as np

def gauss_jordan(a: np.array, b: np.array) -> np.array:
	n = np.size(b)
	
	m = np.hstack((a, b.reshape(-1,1)))
	m = m.astype('float')
	
	for i in range(n):
		# make m[i][i] = 1
		if m[i][i] == 0:
			raise ValueError(f"zero division in m{i}{i}")

		m[i] = m[i] / m[i][i]

		# make m[j][i] for i in 0..n if j != i
		for j in range(n):
			if i == j:
				continue
			
			m[j] = m[j] - m[i]*m[j][i]

	return m.T[n]
```
