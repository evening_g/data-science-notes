This is an iterative method. The first row contains only zeros. The following rows are computed in base to the previous row.

You can find the sum of all the previous row and then just subtract the value where $j=i$.
$$
x_{i}^{k+1} =
\frac{a}{a_{i,i}}

\left(
	b_{i} -
	\sum^{n}_{j=1\ j\ne 1}
		a_{i,j} x_j^k
\right)
$$
$k$ row: $1..\text{max iterations}$
$i$ column
$n$ number of variables

```python
def jacobi(self, a: np.array, b: np.array) -> np.array:
	n = np.size(b)
	x = np.zeros((self.max_k+1, n))

	for k in range(self.max_k):
		for i in range(n):
			x[k + 1][i] = 1/a[i][i] * (
					b[i] - sum(a[i][j] * x[k][j] for j in range(n) if j != i)
				)

		if self.stop_condition(x, k):
			return x[k+1]

	raise ValueError("max_k reached, no roots found")
```
