**Pivot:** Each value $a_{i,i}$ of the matrix.

**Matrix:** Defined $[A|b]$

**Requirements for iterative methods:**

$|a_{i,i}| > |a_{i,j}|$ necessary.
$|a_{i,i}| > \sum{|a_{i,j}|}|$ enough.

**Stop criteria:**

$$
e = |x^{k+1}-x^{k}| < 10^{-3}
$$
