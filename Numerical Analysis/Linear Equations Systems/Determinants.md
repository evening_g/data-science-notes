This method is very slow and often we can't find the solution by this method.

We have to build a matrix the following way:

$$
\begin{bmatrix}
a_{0,0} & a_{0,1} & \dots & a_{0,n} & | & x_0 
\\
a_{1,0} & a_{1,1} & \dots & a_{1,n} & | & x_1 
\\
\vdots & \vdots & \ddots & \vdots & | & \vdots 
\\
a_{n,0} & a_{n,1} & \dots & a_{n,n} & | & x_n 
\end{bmatrix}
$$
1. Find the system determinant.

Multiply all the diagonals and add them Then multiply all the inverse diagonals and substract them.
Example for a $3\times3$ matrix.
$$
\Delta x =
 [(a_{0,0})(a_{1,1})(a_{2,2})]
+[(a_{0,1})(a_{1,2})(a_{2,0})]
+[(a_{0,2})(a_{1,0})(a_{2,1})]
-[(a_{0,2})(a_{1,1})(a_{2,0})]
-[(a_{0,0})(a_{1,2})(a_{2,1})]
-[(a_{0,1})(a_{1,0})(a_{2,2})]
$$

2. Find all the other determinants.

$\Delta a_i$ is found by replacing the $i$-th column with the $x$ vector and finding the determinant.

$\text{For }i \in \{0..n\}$ {
	$x_i = \frac{\Delta a_i}{\Delta x}$
}

Note that if $\Delta x = 0$ the system won't have solution by determinants.

