This is an iterative method. The first row contains zeros. It is often faster than Jacobi.

$$
x_{i}^{k+1} = \frac{1}{a_{i,i}}
\left(
b_i

- \sum^{i-1}_{j=i}
	a_{i,j} x_j^{k+1}

- \sum^{n}_{j=i+1}
	a_{i,j}x_{j}^{k}
\right)
$$

$k$ row: $1..\text{max iterations}$
$i$ column
$n$ number of variables

```python
def gauss_seiden(self, a: np.array, b: np.array) -> np.array:
	n = np.size(b)
	x = np.zeros((self.max_k+1, n))

	for k in range(self.max_k):
		for i in range(n):
			x[k + 1][i] = 1/a[i][i] * (
					b[i] - sum(a[i][j] * x[k+1][j] for j in range(i-1))
						- sum(a[i][j] * x[k][j] for j in range(i+1, n))
				)

		if self.stop_condition(x, k):
			return x[k+1]

	raise ValueError("max_k reached, no roots found") 
```
