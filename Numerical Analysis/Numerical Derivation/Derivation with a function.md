Apply the of the derivation definition:
$$
\lim_{\Delta x \to 0}
\frac{f(x+\Delta x) - f(x)}{\Delta x}
$$
Just choose the smallest $\Delta x$ possible.