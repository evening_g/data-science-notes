When only having a tabular function we can apply the following methods:

$h$ is the distance between $x_i$ and $x_{i+1}$. $h$ must be the same for each adjacent pair of points.

**Forwards:**
$$
f'(x_i) = \frac{f(x_{i+1}) - f(x_i)}{h}
$$

**Backwards:**
$$
f'(x_i) = \frac{f(x_{i}) - f(x_{i-1})}{h}
$$

**Middle:**
$$
f'(x_i) = \frac{f(x_{i+1}) - f(x_{i-1})}{h}
$$

For adding more precision choose more points. These formulas come from the Taylor polynomial.

**Forwards:**
$$
f'(x_i) = \frac{-f(x_{i+2}) + 4f(x_{i+1}) - 3f(x_i)}{2h}
$$

**Backwards:**
$$
f'(x_i) = \frac{3f(x_i) - 4f(x_{i-1}) + f(x_{i-2})}{2h}
$$
**Middle:**
$$
f'(x_i) = \frac{-f(x_{i+1}) + 8f(x_{i+1}) - 8f(x_{i-1}) + f(x_{i-2})}{12h}
$$
