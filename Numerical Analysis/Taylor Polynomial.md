It is used to approximate functions.

$$
P_n(x) =
\sum^\infty_{n=0}
	\frac{f^{(n)}(x_0)}{n!}(x-x_0)^n
$$
Please tell me it's beautiful.
