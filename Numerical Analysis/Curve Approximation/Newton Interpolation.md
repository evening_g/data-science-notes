We have to build the following table. It is very easy to make bigger.

$$
\begin{bmatrix}
x_k & f(x_k) & \textbf{First} & \textbf{Second} & \textbf{Third}
\\
x_0 & f(x_0)
\\
x_1 & f(x_1) & f[x_0, x_1]
\\
x_2 & f(x_2) & f[x_1, x_2] & f[x_0, x_1, x_2]
\\
x_3 & f(x_3) & f[x_2, x_3] & f[x_1, x_2, x_3] & f[x_0, x_1, x_2, x_3]
\end{bmatrix}
$$
Each value in the matrix can be calculated using the following formula.
$$
f[x_{k-j}, x_{k-j+1}, \dots, x_{k}]
=
\frac{f[x_{k-j+1},\dots, x_k] -  f[x_{k-j},\dots, f[x_{k-1}]}{x_k-x_{k-j}}
$$
You can dynamically build up the matrix.

$f[n+1][n+2]$
*copy* $X$ *to column* $0$.
*copy* $Y$ *to column* $1$.
*for* $j \in 2..n+2$ {
	*for* $i \in j-1..n+1$ {
		$f[i][j] = \frac{f[i][j-1]-f[i-1][j-1]}{f[i][0] - f[i-j+1][0]}$
	}
}

Now to build up the polynomial, select the values in the diagonal, starting in $i$ where $x_i$ is the first point you want to take, and finish in $i+n+1$ where $n$ is the degree of the polynomial.

$$
P_n(x) = \sum_{i=0}^{n} \left ( f[i][i+1] \prod_{j=0}^{i}{x-x_j} \right)
$$
**Code:**

```python
import numpy as np
```

```python
def newton_interpolation_matrix(X, Y, n):
    f = np.zeros((n + 1, n + 2))

    f[:, 0] = X
    f[:, 1] = Y

    for j in range(2, n + 2):
        for i in range(j - 1, n + 1):
            f[i][j] = (f[i][j - 1] - f[i - 1][j - 1]) / (f[i][0] - f[i - j + 1][0])

    return f
```

```python
def newton_interpolation_function(X, Y, n):
    f = newton_interpolation_matrix(X, Y, n)

    return lambda x: sum(
        f[i][i + 1] * np.prod([x - X[j]
                               for j in range(0, i)])
        for i in range(0, n + 1))

```
