[Wikipedia](https://en.wikipedia.org/wiki/Spline_(mathematics)).
This is not an interpolation method, in this we create a set of low-degree polynomials (3 or smaller), each interpolating a set of continuous points. This avoids [Runge's phenomenon](https://en.wikipedia.org/wiki/Runge%27s_phenomenon) and is usually faster.

Let $S$ be the set of functions.
Given a set of coordinates $C = [(x_0, y_0), (x_1, y_1), \dots, (x_n, y_n)]$ we have to find a set of $n$ splines $S_j(x) \text{ for } i = 0..n-1$

A spline of degree $n$ is defined as follows:
$$
S(x) = 

\begin{Bmatrix}
x\in[x_0, x_1] \to  S_0(x)
\\
x\in[x_1, x_2] \to  S_1(x)
\\
\vdots
\\
x\in[x_{n-1}, x_n] \to  S_n(x)
\\
\end{Bmatrix}
$$
## Cubic splines

This is the most common type of spline, as it gives the best results in most of the cases, however there are different types of splines.

A cubic spline is defined as follows:

$$
S_i(x) = a_i + b_i(x-x_i)+c_i(x-x_i)^2 + d_i(x-x_i)^3
$$
To compute $a, b, c$ and $d$.

$$
\begin{align*}
h_i &= x_{i+1}-x_i
\\
a_i &= y_i
\end{align*}
$$
$$
A = 
\begin{bmatrix}
1 & 0 & 0 & 0 & \dots & 0
\\
h_0 & 2(h_0+h_1) & h_1 &  0 & \dots & 0
\\
0 & h_1 & 2(h_1+h_2) & h_2  & \dots & 0
\\
\vdots & \vdots & \vdots & \vdots & \ddots & \vdots
\\
0 & 0 & 0 & 0 & h_{n-2} & 2(h_{n-2}+h_{n-1})
\\
0 & 0 & 0 & 0 & \dots & 1
\end{bmatrix}
\begin{bmatrix}
c_0 \\
c_1 \\
c_2 \\
\vdots \\
c_{n-1} \\
c_{n}
\end{bmatrix}
=
\begin{bmatrix}
0 \\
t_1 \\
t_2 \\
\vdots \\
t_{n-1} \\
0
\end{bmatrix}
$$

Where:

$$
t_i = \frac{3}{h_i}(a_{i+1}-a_i)-\frac{3}{h_{i-1}}(a_j-a_{j-1})
$$
Here an algorithm to fill $A$ matrix.

```rust
fill(A, 0)
A[0][0] = 1
A[n][n] = 1

for i in 1..n {
	A[i][i-1] = h[i-1]
	A[i][i] = 2(h[i-1]+h[i])
	A[i][i+1] = h[i]
}

A[n-1][n-1] = h[n-2]
A[n-1][n] = 2(h[n-2]+h[n-1])
```

Solve to find $c$. Find $b$ and $d$.

$$
\begin{align*}
\text{For } x \in 0..n & :
\\
b_i &= \frac{a_{i+1}-a_i}{h_i} - \frac{h_i(t_i-2t_{i+1})}{3}
\\
d_i &= \frac{c_{i+1}-c_i}{3h_i}

\end{align*}
$$
