$$
\begin{bmatrix}
\sum{x^0} & \sum{x^1} & \dots & \sum{x^n} & | \sum{x^0y}
\\
\sum{x^1} & \sum{x^2} & \dots & \sum{x^{n+1}} & | \sum{x^2y}
\\
\vdots & \vdots & & \vdots &    \vdots
\\
\sum{x^n} & \sum{x^{n+1}} & \dots & \sum{x^{2n}} & | \sum{x^ny}
\end{bmatrix}

\Rightarrow

\begin{bmatrix}
a_{n}
\\
a_{n-1}
\\
\vdots
\\
a_{0}
\end{bmatrix}
$$

Solve:

$$
P_n(x) = a_{0}x^{n}+a_{1}x^{n-1}+\dots+a_{n}x^{0}
$$
