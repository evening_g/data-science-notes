Interpolation is a way to approximate curves. We use a set of polynomials of grade $n$. For building a polynomial of grade $n$ we need $n+1$ points.

| X     | Y     |
| ----- | ----- |
| $x_1$ | $y_1$ |
| $x_2$ | $y_2$ |
| $...$ | $...$ |
| $x_n$ | $x_n$ |

It is usually a bad idea to make high degree polynomials, because [Runge's phenomenon](https://en.wikipedia.org/wiki/Runge%27s_phenomenon) thus we don't usually work with polynomials with a higher degree than 3.

![](https://upload.wikimedia.org/wikipedia/commons/thumb/0/0a/Runge_phenomenon.svg/330px-Runge_phenomenon.svg.png)
