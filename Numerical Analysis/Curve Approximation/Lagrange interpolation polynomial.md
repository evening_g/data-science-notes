$$
P_n(x) = \sum^n_{i=0}
{
	\left(
		y_i
		\prod_{j=0,\ j\ne i}^n{
			\frac{x-x_j}{x_i-x_j}
		}
	\right)
}
$$
