For most of these methods, we need to set $[a, b]$ and we will look for the root in that range.

All of these methods are iterative. We need to specify a stop rule, which is usually when:

$$
|x_i -x_{i-1}| < \epsilon
$$
Where $\epsilon$ usually is $10e^{-3}$.

- [Bisection](Bisection.md).
- [Regula Falsi](Regula%20Falsi.md).
- [Fixed Point](Fixed%20Point.md).
- [Newton-Raphson](Newton-Raphson.md).
- [Secant Method](Secant%20Method.md).