$f'(x)$ must exist and be continuous on $[a, b]$.

$$
x_0 = \frac{a+b}{2}
$$
$$
x_i+1 = x_i - \frac{f(x_i)}{f'(x_i)}
$$
## Newton-Raphson on non-linear systems

Set the equations equal to 0.

$$
F = 
\begin{Bmatrix}
f_0(x) = 0\\
f_1(x) = 0\\
\vdots \\
f_n(x) = 0\\
\end{Bmatrix}
$$

**Iterative solving:**

$$
x^{(k+1)} = x^{(k)} - J(x^{(k)})^{-1} F(x^{(k)})
$$
**Where:**

$J$ is the Jacobian matrix.

$$
J(x) = 
\begin{bmatrix}
\frac{\delta f_0}{\delta x_0} & \frac{\delta f_0}{\delta x_1} & \dots & \frac{\delta f_0}{\delta x_n}\\
\frac{\delta f_1}{\delta x_0} & \frac{\delta f_1}{\delta x_1} & \dots & \frac{\delta f_1}{\delta x_n}\\
\vdots & \vdots & \ddots & \vdots\\
\frac{\delta f_n}{\delta x_0} & \frac{\delta f_n}{\delta x_1} & \dots & \frac{\delta f_n}{\delta x_n}\\
\end{bmatrix}
$$
$$
x^{(0)} = (x_0^{(0)}, x_1^{(0)}, \dots , x_n^{(0)})
$$
$J(x^{(k)})^{-1}F(x^{k})$ is the vector result of solving the linear system $[J(x^{(k)}) | F(x^{(k)})]$ by Gauss-Jordan or any other method.

### Stop criteria

Iterate until the euclidean distance between the current solution and the previous one is smaller than the allowed error.

$$
d = \sqrt{\sum{(x_i^{(k)} - x_i^{(k+1)})^2}}
$$
Stop if $d \le \epsilon$.
