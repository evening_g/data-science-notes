Given $[a, b]$ search for the root in the range.

$$
x_0 = b-f(b)
\left(
    \frac{a-b}{f(a)-f(b)}
\right)
$$
Then for $i \in 1, 2, 3, ...$ 

$$
a = \left\{
	\begin{matrix}
		x &\text{ if }f(a) \cdot f(x) > 0
		\\
		a &\text{ if }f(a) \cdot f(x) < 0
	\end{matrix}
\right.
$$

$$
b = \left\{
	\begin{matrix}
		x &\text{ if }f(b) \cdot f(x) < 0
		\\
		b &\text{ if }f(b) \cdot f(x) > 0
	\end{matrix}
\right.
$$

$$
x_i = b-f(b)
\left(
    \frac{a-b}{f(a)-f(b)}
\right)
$$