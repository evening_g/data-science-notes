- [Newton-Raphson on non-linear systems](Newton-Raphson.md#Newton-Raphson%20on%20non-linear%20systems).
- [Fixed Point](Fixed%20Point.md) TODO.

## Stop criteria on non linear systems

Use the euclidean distance for $n$ dimensions.

$$
d(s_{k+1}, s_k) = \sqrt{(s_{1, k+1}-s_{1, k})^2+(s_{2, k+1} - s_{2, k} + \dots + {s_{n, k+1}, s_{n, k}})}
$$
Again, we iterate until $d$ is smaller than a given value.
