Propose $g(x)$ from $f(x)$ such that $g(x) = x$. Make sure it meets the following criteria:

- $g(x)$ is continuous in $[a, b]$.
- It must have a **fixed point**: $g(x*) = x*$.

$$
x_0 = \frac{a+b}{2}
$$

$$
x_{i+1} = g(x_i)
$$
