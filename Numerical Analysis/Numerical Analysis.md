## Index

### Used Libraries for Python
- [NumPy](Must%20Have%20Libraries%20(Python).md#NumPy).
- [SymPy](Must%20Have%20Libraries%20(Python).md#SymPy).

### Function approximation
- [Taylor Polynomial](Taylor%20Polynomial.md).

### Non-Linear equations
- [Solving Non Linear Equations](Solving%20Non%20Linear%20Equations.md).
- [Solving Non-Linear Equations Systems](Solving%20Non-Linear%20Equations%20Systems.md).
- [Bisection](Bisection.md).
- [Fixed Point](Fixed%20Point.md).
- [Newton-Raphson](Newton-Raphson.md).
- [Regula Falsi](Regula%20Falsi.md).
- [Secant Method](Secant%20Method.md).

### Linear equations
- [Basic concepts](Basic%20concepts.md).
- [Determinants](Determinants.md).
- [Gauss (Regressive substitution)](Gauss%20(Regressive%20substitution).md).
- [Gauss-Jordan](Gauss-Jordan.md).
- [Gauss-Seiden](Gauss-Seiden.md).
- [Jacobi](Jacobi.md).

### Curve approximation
- [Splines](Numerical%20Analysis/Curve%20Approximation/Splines.md). 
- [Least squares](Least%20squares.md). TODO
#### [Interpolation](Interpolation.md)
- [Interpolation matrix](Interpolation%20matrix.md).
- [Lagrange interpolation polynomial](Lagrange%20interpolation%20polynomial.md).
- [Newton Interpolation](Newton%20Interpolation.md).

### Numerical Derivation
- [Derivation with a function](Derivation%20with%20a%20function.md).
- [Derivation with a tabular function](Derivation%20with%20a%20tabular%20function.md).
- [n-th derivative](n-th%20derivative.md).

### [Numerical Integration](Numerical%20Integration.md)
- [Trapezoidal Rule](Trapezoidal%20Rule.md).
- [Simpson ⅓](Simpson%20⅓.md).
- [Simpson ⅜](Simpson%20⅜.md)

### Ordinal Differential Equations
- [Solving  Ordinal Differential Equations](Solving%20%20Ordinal%20Differential%20Equations.md).
- [Euler Method](Euler%20Method.md).
- [Taylor Method](Taylor%20Method.md).
- [Runge-Kutta 2nd order](Runge-Kutta%202nd%20order.md).
- [Runge-Kutta 4th order](Runge-Kutta%204th%20order.md).
