The main advantages of this method is that it doesn't require deriving and can be applied to fourth order differential equations.

$$
\begin{matrix}
i & x_i & y_i &
f(x_i, y_i) &
y^*_{i+\frac{1}{2}} &
f(x_{i+\frac{1}{2}}, y^*_{i+\frac{1}{2}}) &
y^{**}_{i+\frac{1}{2}}
\\
\hline
0 & x_0\\
1 & x_1\\
\vdots &\vdots\\
n & x_n
\end{matrix}
$$

**Where:**
$$
\begin{align*}
x_{i+\frac{1}{2}} &= x_i+\frac{h}{2}
\\
y^*_{i+\frac{1}{2}} &= y_i + \frac{h}{2}f(x_i, y_1)
\\
y^{**}_{i+\frac{1}{2}} &= y_i + \frac{h}{2}f(x_{i+\frac{1}{2}}, y^{*}_{i+\frac{1}{2}})
\\
y^*_{i+1} &= y_i + hf(x_{i+\frac{1}{2}}, y^{**}_{i+\frac{1}{2}})
\\
y_{i+1} &= y_i + \frac{h}{6}
\left[
f(x_i, y_i)
+ 2f(x_{i+\frac{1}{2}}, y^*_{i+\frac{1}{2}})
+ 2f(x_{i+\frac{1}{2}}, y^{**}_{i+\frac{1}{2}})
+ f(x_{i+1}, y^*_{i+1})
\right]

\end{align*}
$$
