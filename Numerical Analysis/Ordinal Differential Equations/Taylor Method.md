This is an extension of the [Euler Method](Euler%20Method.md). It also comes from the [Taylor Polynomial](Taylor%20Polynomial.md) but it uses more degrees, still, can only be applied on first order equations.

For this method, use $x = x_0$ and then expand the polynomial:
$$
f(x_{i+1}) = f(x_i)
+ f'(x_i)(x_{i+1} - x_i)
+ \frac{f''(x_i)}{2!}(x_{i+1}-x_i)^2
+ \frac{f'''(x_i)}{3!}(x_{i+1}-x_i)^3
+ \dots
$$
Then:
$$
\begin{matrix}
i & x_i & y_i & y'_i & y''_i & y'''_{i+1} & \dots
\\
\hline
0 & x_0\\
1 & x_1\\
\vdots & \vdots\\
n & x_n
\end{matrix}
$$
As the notation suggests, we must put the expression in function of $x$ which is easily achieved in first order differential equations by integrating the whole equation.