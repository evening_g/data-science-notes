When solving differential equations, the solution is a grid of data. Usually a list of $x$ and $y$.

Usually we are required to approximate a solution within a range over a variable (like $x$) and a step $h$ (difference between each $x$).

The most common methods to solve differential equations are:

**For 1th order equations:**
- [Euler Method](Euler%20Method.md).
- [Taylor Method](Taylor%20Method.md).

**For n-th order equations:**
The Runge-Kutta methods can be adapted to any order, however the 4th is the most popular.

Runge-Kutta methods try to find an equation of the following form.
$$
y_{i+1} = y_i + (w_1k_1 + w_2k_2 + \dots + w_nk_n)
$$

- [Runge-Kutta 2nd order](Runge-Kutta%202nd%20order.md).
- [Runge-Kutta 4th order](Runge-Kutta%204th%20order.md).