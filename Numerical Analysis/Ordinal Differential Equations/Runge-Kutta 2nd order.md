The main advantages of this method is that it doesn't require deriving and can be applied to second order differential equations.

$$
\begin{matrix}
i & x_i & y_i & f(x_i, y_i) & y^*_{i+1} & f(x_i, y^*_{i+1}) & y_{i+1}
\\
\hline
0 & x_0\\
1 & x_1\\
\vdots &\vdots\\
n & x_n
\end{matrix}
$$

**Where:**
$$
\begin{align*}
y^*_{i+1} &= y_i + hf(x_i, y_i)
\\
y_{i+1} &= \frac{h}{2}\left[f(x_i, y_i) + f(x_{i+1}, y^*_{i+1})\right]
\end{align*}
$$
