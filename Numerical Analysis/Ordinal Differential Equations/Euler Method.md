This method is not very precise. It is based on the definition:

$$
f(x_{i+1}) = f(x_i) + f'(x_i)h
$$
This is of course another application of the [Taylor Polynomial](Taylor%20Polynomial.md) using only up to the second term.

It can only be applied to first order equations.

$$
\begin{matrix}
i & x_i & f(x_i) & f'(x_i) & f(x_{i+1})
\\
\hline
0 & x_0\\
1 & x_1\\
\vdots &\vdots\\
n & x_n
\end{matrix}
$$

As the notation suggests, we must put the expression in function of $x$ which is easily achieved in first order differential equations by integrating the whole equation.