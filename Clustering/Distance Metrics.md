There are many distance metrics. Here we can see many of them:

![](Distance%20Metrics.webp)

Many models support different metrics. Changing the used metric affects the results.

## Euclidean

This is the most common and default for most models. It is computed using the Pythagoras theorem. It is not recommended when there are **many dimensions**.

## Square Euclidean

This is the same than the Euclidean but we don't compute the root at the end.

## Manhattan

This is the sum of the cathetus.

## Cheyshev

This is the value of the largest cathetus. It is only used in specific cases.

## Minkowsky

This is the union of different metrics.
$$
D(x, y) = \left( \sum^{n}_{i = 1} |x_i - y_i|^p \right)^\frac{1}{p}
$$
We have to assign a value to $p$.

If $p = 1$: Manhattan.
If $p = 2$: Euclidean.
If $p = \infty$: Chebyshev.

This metric is very useful because we can use a tool like [GridSearchCV](Supervised%20learning.md#GridSearchCV) to find what's the best $p$ value.

## Canberra

This is a weighted version of Manhattan. It works better with atypical data.
$$
D(x, y) = \sum^n_{i=1}\frac{|x_i - y_i|}{(x_i + y_i)}
$$

## Chi-Square

This is used in **texture analysis,** like facial recognition.
$$
D(x, y) = \sum^{n}_{i = 1}\frac{(x_i - y_i)^2}{x_i + y_i}
$$
There's another version used for [Decision Trees](Decision%20Trees.md).

$$
x^2 = \sum_{k}\frac{(\text{Observed}_k - \text{Expected}_k)^2}{\text{Expected}_k}
$$

## Weighted Euclidean

This metric gives different importance (weight) to each characteristic.
$$
D(x, y) = \sqrt{\sum^n_{i=1}(w_i(x_i-y_i))^2}
$$
$w_i$ is the weight of the $i$-th characteristic.

## Cosine

This metric works better when we have **too many dimensions.** It measures the angle between two vectors, but the vectors magnitude is not taken in account.

## Mahalanobis distance

TODO