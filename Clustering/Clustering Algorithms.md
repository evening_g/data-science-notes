## Distribution based

It is based in shapes.

**Example:**

- [Gaussian Mixtures](Gaussian%20Mixtures.md).

## Density based

It is based in the density of de points. It forms clusters with given density.

**Examples:**

- [DB-Scan](DB-Scan.md).

## Centroids based

It finds clusters based on centroids. It is fast and efficient and works in most of the cases.

**Examples:**

- [K-Means](K-Means.md).

## Hierarchies based

These builds groups based in trees, being the full data the tree root, the more levels down, the more specific the groups.

**Examples:**

- [Hierarchical Clustering](Hierarchical%20Clustering.md).