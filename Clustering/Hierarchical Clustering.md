This is an [unsupervised](Unsupervised%20Learning.md) clustering algorithm. This algorithm is used when we have **unlabeled data**. If we know the classification we should use another algorithm.

We must take in account that the hierarchical algorithm doesn't work well with **huge amount of data.**

The algorithm creates a tree with the data. Each leaf is a cluster. The root contains all the data. The more levels we go down, the more specific are the clusters. The tree is called *dendrogram.*

## Divisive clustering

It starts with a single global cluster which is divided recursively. This is very slow.

## Distance metrics

This approach needs to know the distance between the clusters. There are different metrics.

- **Minimum connection:** This is the distance between the most similar points in the clusters.
- **Maximum connection:** This is the distance between the least similar points in the clusters.
- **Centroid connection:** This is the distance between the centroids of the clusters.
- **Mean connection:** This is the mean of the distance between each pair of the cluster.

## Usage

### Find the right number of clusters

You can easily do this with a dendrogram.

```python
import scipy.cluster.hierarchy as sch

dendrogram = shc.dendrogram(sch.linkage(df))
```

https://docs.scipy.org/doc/scipy/reference/cluster.hierarchy.html

![Dendrogram](Dendrogram.png)

Now you have to find the horizontal line that intersects the most possible longest vertical lines. n the image above, you should choose 5 groups, if you draw a horizontal line at 150.

### Agglomerative clustering

It starts with each point being a cluster. The clusters are agglomerated with their most similar neighbors until we have the desired number of clusters. This is the most common.

```python
from sklearn.cluster import AgglomerativeClustering

# N number previously found
# METRIC distance metric to use
# LKG Which linkage criterion to use. (See link below)

model = AgglomerativeClustering(n_clusters = N, metric = METRIC, linkage = LKG)
```

https://scikit-learn.org/stable/modules/generated/sklearn.cluster.AgglomerativeClustering.html
