It's the square root of [variance](Variance.md). Refers of how far are the values from the median.

**Note:** $x_i -\overline{x}$ is called **deviation from the median.**

$$
\delta =
\sqrt{
	\frac{
		\sum(x_i - \overline{x})^2
	}{N}
}
$$
$$
s =
\sqrt{
	\frac{
		\sum(x_i - \overline{x})^2
	}{n-1}
}

$$
