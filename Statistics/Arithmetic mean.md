Sample mean.

$$
\overline{x}=\frac{\sum_{i=1}^{n}x_{i}}{n}
$$
**Grouped data.**

Let $k$ be the number of classes.
Let $n_j$ be the absolute frequency of the class $j$.
Let $x_j$ be the mark of the class $j$.
$$
\overline{x} =
\frac{
	\sum^k_{j=1}n_j x_j
}{
n
}
$$
