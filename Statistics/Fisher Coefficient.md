**Grouped data:**

$$
A_F = \frac{
	\frac{1}{n}
	\sum^{k}_{i=1}(x_i-\overline{x})^3
	n_i
}{
	\delta
}
$$

Where:
$A_F$ Fisher asymmetry (coefficient)
$n$ sample size
$x_i$ mark of class
$\overline{x}$ [median](Median.md)
$n_i$ class size
$\delta$ [standard deviation](Standard%20Deviation.md)
