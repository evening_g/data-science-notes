It's the square of [standard deviation](Standard%20Deviation.md). Refers of how far are the values from the median.

**Note:** $x_i -\overline{x}$ is called **deviation from the median.**

$$
\delta^2 =
	\frac{
		\sum(x_i - \overline{x})^2
	}{N}
$$
$$
s^2 =
\frac{
	\sum(x_i - \overline{x})^2
}{n-1}
$$
For **grouped data**.

$$
s^2 = \frac{
	\sum^k_{j=1}n_j(x_i-\overline{x})^2
}{
n-1
}
$$

With [Pandas](Must%20Have%20Libraries%20(Python).md#Pandas) use `var()` method.

See also: [Variance Coefficient](Variance%20Coefficient.md).