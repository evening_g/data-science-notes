## Correlation

Correlation between columns $A$ and $B$ is a coefficient that indicates how much $A$ is related to $B$.

We should remove columns with high correlation, as we want to reduce the number of variables in our models, and if $A$ and $B$ have high correlation, the information that $A$ can provide, can also be provided by $B$.

The maximum correlation we should accept is $|0.7|$.

If you only want the correlation from two columns, you can use the [SciPy](Must%20Have%20Libraries%20(Python).md#SciPy).

```python
from scipy import stats

stats.pearsonr(a, b)
```
## Visualizing the correlation

We can use a [Correlation Matrix](Correlation%20Matrix.md). This will show the Pearson correlation of all the columns against all columns.

```python
# You know what to do
df.corr()
```

In R we can use:

```R
cor(R)
```