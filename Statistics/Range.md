The max value - min value of the data. It's very susceptible to outliers.

In R you can easily find it with the `range` function.