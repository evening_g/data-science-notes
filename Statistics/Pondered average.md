Gives different importance (weights) to each value.

$$
\overline{x}_{p} = \frac{\sum_{i=1}^{n}x_i\cdot w_i}{\sum_{i=1}^{n} w_i}
$$

For example, we can give more importance to the sells in weekends, so we give more weight to the Friday, Saturday and Sunday than the rest of the days.