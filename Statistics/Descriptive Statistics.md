Are used to describe data.

**Tip:** Greek characters are used when we're talking of all the data. Roman letters when we're talking of a sample.
## Central tendency measurements

- [[Arithmetic mean]]
- [[Pondered average]]
- [[Median]]
- [[Mode]]

## Location measurements

- [[Deciles]]
- [[Quartiles]]
- [[Percentiles]]

## Dispersion measurements

- [[Variance]]
- [[Standard Deviation]]
- [Variance Coefficient](Variance%20Coefficient.md)
- [[Range]]
- [[Interquantile Range]]
- [[Pearson Correlation]]

## Shape measurements

- [[Asimetry]]
- [Fisher Coefficient](Fisher%20Coefficient.md)
- [[Kurtosis]]
- [Boxplot](Boxplot.md)
