
This is easier to understand as it is independent of the values.
$$
\left(
	\frac{s}{\overline{x}}
	\cdot
	100
\right)\%
$$
In [Pandas](Must%20Have%20Libraries%20(Python).md#Pandas) do:

```python
df.var() / df.mean()
```
