Returns the value at the middle of the sorted data.

$$
\sim x = 
\left\{
	\begin{matrix}
		n = &\text{odd} \left( \frac{n+1}{2} \right)^{th}\text{sorted value}
		\\
		n = &\text{even}
			\left(
				\frac{\frac{n}{2} + \frac{n+1}{2}}{2}
			\right)
				^{th}\text{sorted value}
	\end{matrix}
\right.
$$

Median is very susceptible to outliers. Sometimes we use  cropped median instead (cut top x% and lowest % of the data).

**Grouped data.**

$$
M = L_c + A_c \frac{\frac{n}{2}-N_{c-1}}{n_c}
$$
Where:
$L_c$ class mark
$A_c$ class range
$n$ sample size
$N_{c-1}$ cumulative sum to the prev class
$n_c$ absolute frequency of the class