**Grouped data**

$$
g_2 = \frac{
	\frac{1}{n}
	\sum^{k}_{i=_1}
	(x_i - \overline{x})^4
}{
\delta ^ 4
}
$$

Where:

$g_2$ kurtosis coefficient
$n$ sample size
$x_i$ mark of class
$\overline{x}$ [median](Median.md)
$n_i$ class size
$\delta$ [standard deviation](Standard%20Deviation.md)

if $g_2 = 3$ => Mesokurtic
if $g_2 > 3$ => Leptokurtic
if $g_2 < 3$ => Platykurtic

