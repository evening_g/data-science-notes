It's similar to the [Deciles](Deciles.md) but dividing the data in 100 parts.

The 1th percentile is the min.
The 25th percentile is the 1th [quartile](Quartiles.md).
The 50th percentile is the [median](Median.md)j
The 75th percentile is the 3th [quartile](Quartiles.md).
The 100th percentile is the max.

**Grouped data.**

$$
P_p = L_c + A_c
\frac{
	np-N_{c-1}
}{
	n_c
}
$$

Where:

$c$ = First class with $\text{cumulative frequency}$ $> p$.
$L_c$ = Class mark.
$A_c$ = Class range.
$n$ = Sample size
$N_{c-1}$ = Cumulative frequency to the previous class.
$n_c$ = Size of the class.
$p$ = Percentile to find, i.e. 0.25 for first quantile.