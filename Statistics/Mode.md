It is the data that repeats the most in the data. We can have multiple modes.

## [R](Basic%20R%20functions.md#Mode)

We can use the library `"modeest"`. We could also create our own function.

## [Pandas](Must%20Have%20Libraries%20(Python).md#Pandas)

You can use the `mode()` method of the `pandas.DataFrame`.
