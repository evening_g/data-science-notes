It's splitting the data in 4 sections with equal amount of values.

The 1th quartile contains the 25% of the data.
The 2th quartile contains the 25% to 50% of the data, and also the [median](Median.md).
The 3th quartile contains the 50% to 75% of the data.
The 4th quartile contains the 75% to 100% of the data.

See also:
- [Deciles](Deciles.md).
- [Percentiles](Percentiles.md).
- [Interquartile Range](Interquantile%20Range.md).

