A boxplot is a type of plot to show the distribution of numeric data. An alternative is the [Violin Plot](Violin%20Plot.md).

The line inside the box represents the [median](Median.md), some plots also include the [quartiles](Quartiles.md). The whiskers represent $1.5$ times the [interquantile range](Interquantile%20Range.md).

```text
     Q1-1.5IQR   Q1   median  Q3   Q3+1.5IQR
                  |-----:-----|
  o      |--------|     :     |--------|    o  o
                  |-----:-----|
flier             <----------->            fliers
                       IQR
```
## [Pandas](Must%20Have%20Libraries%20(Python).md#Pandas)

https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.boxplot.html

```python
import pandas as pd

# df = dataframe

df.boxplot(column=["column(s) to plot"])
```

## [Matplotlib](Must%20Have%20Libraries%20(Python).md#Matplotlib)

https://matplotlib.org/stable/api/_as_gen/matplotlib.pyplot.boxplot.html

```python
import matplotlib.pyplot as plt

plt.boxplot(df['column(s) to plot'])
```

## [Seaborn](Boxenplot.md)

## R

```r
boxplot(df$x)
```
