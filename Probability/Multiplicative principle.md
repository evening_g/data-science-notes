If an event happens of $|E_1|$ ways, and another of $|E_2|$ ways. The two events can happen at $|E_1|\times |E_2|$ ways.

The main difference with the [Additive Principle](Additive%20Principle.md) is that event $|E_2|$ is a subtree of $|E_1|$.