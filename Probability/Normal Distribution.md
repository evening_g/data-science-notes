---
aliases:
  - Gaussian Distribution
---
The Normal Distribution is the distribution that all things tend to. Usually when we have more than $30$ elements, the sample tends to have a normal distribution.

Find the value in the [Normal Distribution Table](Normal%20Distribution%20Table.pdf). Sometimes we need to interpolate between 2 points if we want the exact value. It is very simple and intuitive, use a rule of three.

In the normal distribution, the data in the range $\mu -\delta$ to $\mu + \delta$ contains $0.68$ of the data.
The range $\mu -2\delta$ to $\mu + 2\delta$ contains $0.95$ of the data.
Practically all the data is located in a range of $\pm 4\delta$.

In $z$ scale, $\mu + z\delta$ This is also called [z-score](Vocabulary.md#ZScore).

There is a way to compute the [Normal Distribution Table](Normal%20Distribution%20Table.pdf) with the following formula:

$$
\huge f(x|\mu, \sigma) = \frac{1}{\sigma \sqrt{2\pi}}e ^ {-\frac{1}{2}\left(\frac{x - \mu}{\sigma} \right)^2}
$$
We can formalize this formula for higher dimensions:

$$
\huge
f(x_1, \dots x_k) = \frac{e^{-\frac{1}{2}(\vec{x} - \vec{\mu})^T\Sigma^{-1}(\vec{x} - \vec{\mu})}}{\sqrt{(2\pi)^k |\Sigma|}}
$$

The exponent of $e$ is the [[Distance Metrics#Mahalanobis distance|Mahalanobis distance]]. and $\Sigma$ is the [[Covariance Matrix]].

## Normalize the data

The **normal distribution** requires $\mu = 0$ and $\delta = 1$. If this is not the case, we have to normalize the data.

$$
\text{Normalized data } = z = \frac{x_i-\mu}{\delta}
$$

In the normal distribution.
Let $\delta$ be the standard deviation.
Let $\mu$ be the mean.