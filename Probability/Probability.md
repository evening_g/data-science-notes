**A priori**
We don't need a sample.
$$
P(A) = \frac{\text{Number of ways that }A\text{ can happen}}{\text{Number of simple different events}}
$$

**A posteriori**
We need a sample.
$$
P(A) = \frac{\text{Number of times that }A\text{ happened}}{\text{Number of simple different events}}
$$

## Axioms

Let $S$ be a sample space composed by a set of simple events $s_i$.

$$
S = \{s_0, s_1, \dots s_n\}
$$
Then:

$$
P(S) = \sum{P(s_i)} = 1
$$
$$
0 < P(s_i) < 1
$$
## Counting methods

- [Multiplicative principle](Multiplicative%20principle).
- [Permutations](Permutations.md).
- [Combinatorics](Combinatorics.md).
- [Factorial rule](Factorial%20rule.md).

## Distribution

- [Normal Distribution](Normal%20Distribution.md).
