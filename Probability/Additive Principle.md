An event $A$ formed by a set of events $Ai$ can happen of the following different ways:
$$
A = \sum{A_i}
$$
The main difference with the [Multiplicative principle](Multiplicative%20principle.md) is that $A_1$ and $A_2$ are siblings in the tree diagram.
