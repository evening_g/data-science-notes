Taking $k$ from a set of $n$. Order doesn't matter, no repeated.

$n$ = size of the population
$k$ = size of the sample
$_nP_n = n!$

**With repeated elements**
$$
n^k
$$
**Without repeated elements**
$$
\frac{n!}{(n-k)!}
$$
**Without repeated, $n=k$**
$$
n!
$$

## Permutations with repeated elements

From a set of size $n$ with repeated elements.
$n$ = size of the sample
$x_i$ = number of $i$ repeated elements.

$$
_nP_{x_1!,\ x_2!,\ \dots x_n!}
=
\frac{n!}{x_1!,\ x_2!,\ \dots x_n!}
$$
