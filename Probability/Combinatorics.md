Taking $k$ from a set of $n$. Order matters, no repeated.

$n$ = size of the population
$k$ = size of the sample

$$
{n\choose k}=
\frac{n!}{k!(n-k)!}
$$
$$
{n\choose 0}= 1
$$
