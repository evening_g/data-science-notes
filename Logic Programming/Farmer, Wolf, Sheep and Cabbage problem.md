If you have problems understanding this you can checkout [[Prolog]].

```prolog
% This is an implementation of the problem of the wolf, sheep and cabbage.
% Made by me on july 2023.
%
% If you don't know the problem yet you either shouldn't be here or you forgot it.
%
% So, you have to cross a river with a wolf, a sheep and a cabbage,
% however there are restrictions.
%  - You can move only ONE thing at a time.
%  - The wolf and the sheep CAN'T be together, as the wolf might eat the sheep
%  - The sheep and the cabbage CAN'T be together as the sheep eats the cabbage
% 
% ====================================================================================
%
% We are going to start by defining the states
% 
%  state(You, Wolf, Sheep, Cabbage).
% 
% The start state is when everyone is at the left bank of the river:
%
%  state(l, l, l, l).
%
% And the end state is when everyone is at the right bank.
%
%  state(r, r, r, r).

goal(state(r,r,r,r)).

% ====================================================================================
% 
% Now we're going to define the forbidden states.

forbidden(state(You, Wolf, Sheep, Cabbage)) :- Wolf = Sheep, You \= Sheep; Sheep = Cabbage, You \= Sheep.

% ====================================================================================
%
% Now we're going to define de possible movements


possible(state(l, W, S, C), state(r, W, S, C)) :- not(forbidden(state(r, W, S, C))).
possible(state(r, W, S, C), state(l, W, S, C)) :- not(forbidden(state(l, W, S, C))).

possible(state(l, l, S, C), state(r, r, S, C)) :- not(forbidden(state(r, r, S, C))).
possible(state(r, r, S, C), state(l, l, S, C)) :- not(forbidden(state(l, l, S, C))).

possible(state(l, W, l, C), state(r, W, r, C)) :- not(forbidden(state(r, W, r, C))).
possible(state(r, W, r, C), state(l, W, l, C)) :- not(forbidden(state(l, W, l, C))).

possible(state(l, W, S, l), state(r, W, S, r)) :- not(forbidden(state(r, W, S, r))).
possible(state(r, W, S, r), state(l, W, S, l)) :- not(forbidden(state(l, W, S, l))).

% ====================================================================================
%
% Define some functions we will use

is_member(X, [X | _]).
is_member(X, [_ | L]) :- is_member(X, L).

% ====================================================================================
%
% After defining all the rules, let's go to the part that actually solves the problem.
%
% So, we want a solution:
%
% solution(state(l,l,l,l), Movements).
%
% Were movements will be the list of all the movements (or rather states)
% that you have to follow
% to succesfully moving your burden from one side to another
% without losing any part of it.
% 
% We will define this recursively

% Base case 1. The current state is goal state
solution([CurrentState | L], [CurrentState | L]) :-
    goal(CurrentState),!.

solution([CurrentState | Attempts], Movements) :-
    possible(CurrentState, NewState),
    not(is_member(NewState, Attempts)),
    solution([NewState, CurrentState | Attempts], Movements).
```