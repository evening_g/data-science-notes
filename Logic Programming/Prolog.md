Prolog is a weird language. It is, however, the flagship of the Logic Programming paradigm.

You can see an example of how weird this is here [[Farmer, Wolf, Sheep and Cabbage problem]]. This is one of the programs I've been most proud of in my whole career.

To program in Prolog, you have to create a `.pl` file. In this file you can define relations between things.

```prolog
% Coments start with `%`

% A actions on B
action(A, B).

```

Then you can open a file using:

```bash
swipl file.pl
```

Here you can ask questions.

If you update the file you can reload the file with:

```swipl
make.
```

## Syntax

Variables start with uppercase.
Constants start with lowercase.
Each sentence ends with a `.`.
Comments start with `%`.

### Facts

Mangos are fruits:

```prolog
fruit(mango).
```

### Relations

I love mangos.

```prolog
love(i, mango).
```

### Rules

These are conditional sentences.

X is a if X is b

```prolog
a(X) :- b(X).
```

### Operators

| Operator       | Symbol                                |
| -------------- | ------------------------------------- |
| And            | Comma: `,`                            |
| Or             | Semicolon: `;`                        |
| Unification    | `=`                                   |
| No unification | `\=`                                  |
| Identity       | `==` It is more restrictive than `=`. |
| No identity    | `\==`                                 |
| Negation       | `not(X)`                              |
| Void           | `_`                                   |
| Append         | `|`                                   |
| Cut            | `!`                                   |
| Assign         | `is`                                  |

#### Pipe operator

The `|` operator is used to append an element at the beginning of the list.

```prolog
% Append 'a' at the beginning of the list `L`
[a | L]
``
```

#### Cut operator

The `!` operator is used to stop the execution. For example if we want to know the max of two numbers we can do:

```prolog
% Cut the execution if X is bigger
max_element(X,Y,X) :- X > Y, !.

% If the execution continued, then Y is bigger
max_element(X,Y,Y).
```

### Lists

If you thought that prolog couldn't get weirder... well, it does.

Lists are defined with a head and a tail. They are defined recursively.

```prolog
% empty list
[]

% singleton list
[a]

% long list
[[a,b,c], 1, 2, a]
```

#### List operations

##### Lenght

```prolog
% Base case
list_length([], 0).

% Recursive case
list_length([_ | L], N) :-
	list_length(L, N1),
	N is N1 + 1.
	
% Query
?- list_length([1, 2, 3], N).
N = 3
```

Ok, let's cut this in pieces.

This is the base case. If the list is empty then the size is 0.

```prolog
list_length([], 0).
```

[_ | L] splits the list in [head | tail], as we don't care about the head we use the underscore `_` to discard it. This expression returns `true` if the list can be split in head and tail, so it will return true as long as the list is not empty.

```prolog
list_length([_ | L], N) :-
```

Then it uses recursion to calculate the size of the tail.

```prolog
	list_length(L, N),
```

After the recursion, it has the `N1` value. We increment it by one and assign it to `N`.

```prolog
	N is N1 + 1.
```

##### Determine if an element belongs to a list

After processing the later trauma, this should be pretty simple. If you still don't understand what is this doing, re-read the previous section or go ask Chat-GPT as if you were a 5 yo child. (That's what I did actually).

```prolog
is_member([Head | L], X) :- Head = X.
is_member([_ | L], X) :- is_member(L, X).

% Query:
?- is_member([1, 2, 3], 1).
true .
```

##### Join two lists

I treated you badly last section, but now I had to ask Chat-GPT again.

<iframe style="border-radius:12px" src="https://open.spotify.com/embed/track/4JRem7xHp2l0kmUvt9zCKu?utm_source=generator" width="100%" height="152" frameBorder="0" allowfullscreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture" loading="lazy"></iframe>

```prolog
append_list([], L2, L2).
append_list([X | L1], L2, [X | L3]) :- append_list(L1,L2,L3).
```

This is the base case. When the first list is empty. If we don't put this, we won't be able to stop the recursion.

```prolog
append_list([], L2, L2).
```

Now we split the first list, being `X` the head and `L1` the tail. We do the same with `L3`. **As `X` is already defined, instead of splitting `L3` we add `X` at the beginning.**

```prolog
append_list([X | L1], L2, [X | L3])
```

Then we use recursion. This will be repeated until `L1` is exhausted and we fall back to the base case.

```prolog
 :- append_list(L1,L2,L3).
```

##### Delete the first occurrence of X in L

```prolog
% base case 1

% the list is empty
% We ignore the element to remove and return an empty list
delete_element(_, [], []).

% base case 2

% the head is the element we're looking for
% You can remove the `!` if you want to delete all the occurrences
% for some reason this doesn't insert `X` 
delete_element(X, [X | L], L) :- !.

% recursive case

delete_element(X, [Y | L], [ Y | LR]) :- delete_element(X, L, LR).
```

