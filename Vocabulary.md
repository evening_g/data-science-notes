### Overfitting

Overfitting or **overtraining** happens when, for various reasons, our model is only able to predict the training data, but fails to predict any new data.

## Bias

## Variance

It is a measure of how far observed values differ from the average predicted values.

## Correlation

[[Pearson Correlation]]

## ZScore

See [Normal Distribution](Normal%20Distribution.md). It describes how far from the median is a data, given in [standard deviations](Standard%20Deviation.md) units.

## Bootstrapping