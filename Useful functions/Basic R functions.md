
We can use the [rseek.org](www.rseek.org) website to find libraries for what we need.

## Getting the data

Create a vector.

```r
data <- c(51,53,41,44,57,47,51,44,57)
```

Reading from CSV.

```r
data <- read.csv(<filename>)
```

In RStudio you can also click in import data. The data is automatically imported to a dataframe and stored in a variable `data`.

## Dataframes

These are just like Pandas dataframes. **Access a column** with `data$column`.

## Defining functions

```r
function_name <- function (args) {
	return(value)
}
```

## Frequencies table

```r
R = diff(range(data))

# N = number of bins
data_table <- table(cut(data, breaks = N))
```

## Plots

```r
plot(data_table)
```

Usually the plot in R interprets what's the best plot to use for the given dataset.

```r
pairs(df)
```

## Accumulated frequency

```r
ac_freq <- cumsum(data)
```

## Relative frequency

```r
rel_freq <- data/sum(data)
```

### Correlation

```R
cor(df)
```


## Mode

To find the mode in R we need to install a package called `"modeest"` which contains the function `mlv()`.

```r
install.packages("modeest")

library(modeest)

# ...

mlv(data, method="mfv")
```

## Range

Returns max - min of the data.

```r
range(data)
```