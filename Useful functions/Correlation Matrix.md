This is a matrix of all against all with values between -1 and 1.

 - 1: Strong positive correlation.
 - 0: No correlation.
 - -1: Strong negative correlation.

It is helpful to build a [heat map](Heatmap.md) based on the matrix data.

## [Pandas](Must%20Have%20Libraries%20(Python).md#Pandas)

This is again the easiest way.

```python
correlation_matrix = df.corr()
```

We can also use the **tidy format** when there are a lot of columns. This format shows only the columns with high correlation.

```python
def tidy_corr_matrix(corr_mat) -> pd.DataFrame:  
    """  
    Función para convertir una matriz de correlación de pandas en formato tidy.    """  
    corr_mat = corr_mat.stack().reset_index()  
    corr_mat.columns = ['A','B','Corr']  
    corr_mat = corr_mat.loc[corr_mat['A'] != corr_mat['B'], :]  
    corr_mat['|Corr|'] = np.abs(corr_mat['Corr'])  
    corr_mat = corr_mat.sort_values('|Corr|', ascending=False, ignore_index=True)  
  
    return corr_mat

tidy = tidy_corr_matrix(df.corr())
```

## [R](Must%20Have%20Libraries%20(R).md)

It is included by default.

```R
cor(df)
```
