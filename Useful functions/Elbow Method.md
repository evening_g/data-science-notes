This method consists in creating a plot of the cumulative explained variance ratio and finding the "elbow" in the plot. Sometimes there is no elbow so you have to fall back to other methods.

```python
import numpy as np
import matplotlib.pyplot as plt

expl = pca.explained_variance_ratio_

plt.plot(np.cumsum(expl))
plt.xlabel("Number of components")
plt.ylabel("Cumulative explained variance")
```

![](Elbow%20Method.png)
Here you can see that the "elbow" is when there are two components. So these are the components we could take.