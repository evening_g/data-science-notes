It is useful to know the percentage of each thing there is. Available in [Pandas](Must%20Have%20Libraries%20(Python).md#Pandas).

```python
import pandas as pd

pd.crosstab(data['column1'], data['column2'])
```