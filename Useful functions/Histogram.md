Think of bucket sort. When you have data, (categorical or numerical), you can put it into bins and count how many objects are there in a bin.

This can be used as a replacement to a [density plot](Density%20plot.md).

## [Pandas](Must%20Have%20Libraries%20(Python).md#Pandas)

This is the easiest way.

https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.hist.html

```python
# N is the number of bins to use
df['column'].hist(bins = N)
```

## [Matplotlib](Must%20Have%20Libraries%20(Python).md#Matplotlib)

This is the most powerful way.

https://matplotlib.org/stable/api/_as_gen/matplotlib.pyplot.hist.html

You must bin the data yourself using [`numpy.histogram`](Must%20Have%20Libraries%20(Python).md#NumPy)

```python
counts, bins = np.histogram(x)
plt.stairs(counts, bins)
# or
plt.hist(bins[:-1], bins, weights=counts)
```
## R

```r
hist(df, xlab="xcol", col="color", border="border_color")
```
