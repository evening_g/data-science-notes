This is a very powerful graphic that compares all the variables to each other. It is useful to visually find correlation between columns.

```python
import seaborn as sns

sns.pairplot(df)
```