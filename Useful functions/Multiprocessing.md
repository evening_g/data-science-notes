This is a built-in Python library.

https://docs.python.org/3.11/library/multiprocessing.html

Some algorithms support parallel computing, this library provides you with tools to help you with that.

```python
import multiprocessing
```

## `cpu_count()`

Returns the number of CPUs.