## tidyverse

https://www.tidyverse.org/

This library provides a lot of packages for data science.

```r
install.packages("tidyverse")
```

### ggplot2

https://ggplot2.tidyverse.org/

This is a tidyverse package for graphics, if you already have tidyverse you don't have to install anything.

You can install it without tidyverse.

```r
install.packages("ggplot2")
```


