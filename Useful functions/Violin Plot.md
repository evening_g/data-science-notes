It's similar to the [boxplot](Boxplot.md) but also shows the distribution. Available in [Seaborn](Must%20Have%20Libraries%20(Python).md#Seaborn).

```python
import seaborn as sns

sns.violinplot(x='column1', y='column2', data=df)
```

