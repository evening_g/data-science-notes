This is a list of libraries you must have as a Data Scientist.

## Installing libraries

It is generally a good idea to have virtual environments. If you're using very specific libraries for a project, better create a new environment.

### Conda

This package manager makes easy to manage dependencies, with the take off that some packages are not available here, anyways, there are workarounds.

First try to add the `conda-forge` channel.

```bash
conda config --add channels conda-forge
```

If it doesn't work, install the package with pip, within your conda environment.

```bash
conda activate YourEnvironment
conda install pip
pip install YourPackage
```

Only use this when the package is not available in conda.

### Python Venv

## Jupyter

## NumPy

## SymPy

## Pandas

This provides powerful data structures and some useful functions.

```python
pip install pandas
```

```python
import pandas as pd
```

## Matplotlib

This is very useful for building plots.

https://matplotlib.org/stable/index.html

```python
pip install matplotlib
```

```python
import matplotlib.pyplot as plt
```

Creating multiple plots with matplotlib. N rows, M columns.

```python
fig, ax = plt.subplots(nrows=N, ncols=M, figsize=(20,10))  
ax[0].plot() #plot 1
ax[1].plot() #Plot 2
plt.show()
```
## scikit-learn

This library has a huge amount of tools useful for Machine Learning and data science in general.

https://scikit-learn.org/stable/index.html

```python
pip install scikit-learn
```

```python
import sklearn as sk
```

## Seaborn

This library provides methods for statistical data visualization. It's plots look more pretty than matplotlib and it's compatible with it.

https://seaborn.pydata.org/

```python
pip install seaborn

import seaborn as sns
```

## Keras

https://keras.io/api/

This is a really good library that provides deep learning tools. It also allows us to save and read neural networks so we don't need to train them every time. It requires of tensorflow to work.

```python
conda install keras
```

## Tensorflow

This is one of the best libraries for machine learning. It supports GPU acceleration.

```bash
conda install tensorflow
# or
pip install tensorflow
```

## pyDataset

This library provides a lot of datasets for educational purposes.

https://pydataset.readthedocs.io/en/latest/

```python
pip install pydataset
```

```python
from pydataset import data
```

## JoyPy

This library provides joyplots.

https://github.com/leotac/joypy

```python
pip install joypy
```

## SciPy

This library contains a collection of mathematical algorithms built on NumPy.

```python
pip install scipy

import scipy
```

## VizNet

This library provides various tools for visualizing neural networks and other types of data.

It is not available in conda.

```python
pip install viznet
```

