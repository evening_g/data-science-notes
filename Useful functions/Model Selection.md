## GridSearchCV

https://scikit-learn.org/stable/modules/generated/sklearn.model_selection.GridSearchCV.html

```python
from sklearn.model_selection import GridSearchCV
import multiprocessing

# Specify the names of the paramenters we are going to test
# And the values we want to try
parameters = {
    "parameter_1" : range(1,20),
    "parameter_2" : ["value_1", "value_2"]
}

gridsearch = GridSearchCV(
	estimator = model_initialization(),
	param_grid = parameters,
	# For multiprocessing
	n_jobs = multiprocessing.cpu_count()-1 #-1 uses all
)
gridsearch.fit(x_train_pca, y_train)

# Show the best params
gridsearch.best_params_

best_model = gridsearch.best_estimator_
```

This function allows [Multiprocessing](Multiprocessing.md).

The results of the test are found in `gridsearch.cv_results_`, this can be turned into a dataframe to be plotted.

For example:

```python
scores = pd.DataFrame(gridsearch.cv_results_)
scores.plot(x='parameter to plot', y='mean_test_score')
```

## RandomizedSearchCV

Very similar to the previous.

https://scikit-learn.org/stable/modules/generated/sklearn.model_selection.RandomizedSearchCV.html

```python
from sklearn.model_selection import RandomizedSearchCV
```

Not all parameters are tried, only a fixed number, set with `n_iter`.