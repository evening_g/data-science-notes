This are like [Boxplot](Boxplot.md) but have more divisions. Available in [Seaborn](Must%20Have%20Libraries%20(Python).md#Seaborn).

```python
import seaborn as sns

sns.boxenplot(x=data_x, y=data_y)
```